package com.softdragon.omniwatch.library.presenter;

/**
 * Created by Dawid on 2016-03-24.
 */
public class DefaultColor {
    public static final float DEFAULT_HUA = 61.0f;
    public static final float DEFAULT_SATURATION = 100.0f;
}

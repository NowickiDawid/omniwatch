package com.softdragon.omniwatch.library.presenter;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.R;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.group.BitmapGroupElement;
import com.softdragon.omniwatch.library.view.element.MultiBitmapMultiStateGraphicElement;
import com.softdragon.omniwatch.library.view.element.OneBitmapMultiStateGraphicElement;
import com.softdragon.omniwatch.library.view.element.SimpleBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleRoundeBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleSquareBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.battery.BatteryLevelGraphicDescription;
import com.softdragon.omniwatch.library.view.element.counter.CounterGraphicElement;
import com.softdragon.omniwatch.library.view.element.group.description.GroupGraphicDescription;
import com.softdragon.omniwatch.library.view.element.text.SimpleTextGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.tips.CustomDrawableTipsGraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPartUrl;
import com.softdragon.omniwatch.library.view.element.utils.ColorFilterGenerator;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Dawid on 2016-02-11.
 */
public class WatchFace implements WatchFaceBase {
    private final OmniWatchView mOmniWatchView;
    private BackgroundGraphicElement mBackground;
    private Paint mPaint;
    private Paint mPaintText;

    private Paint mPaintTextDescription;
    private BatteryLevelGraphicDescription batteryLevelGraphicDescription;
    private Paint mPainAmbition;

    public WatchFace(OmniWatchView omniWatchView) {
        mOmniWatchView = omniWatchView;
    }

    @Override
    public BackgroundGraphicElement createRoundBackground() {
        return createBackground(getURl("background"));
    }

    private String getURl(String url) {
        return String.format("builder-mipmap/%s.png", url);
    }

    @Override
    public BackgroundGraphicElement createSquareBackground() {
        return createBackground(getURl("background_square"));
    }

    @Override
    public GraphicElement[] createRoundNormalMode() {
        return createMode(createMinuteBackground(getURl("nexo_paski")));
    }

    @Override
    public GraphicElement[] createSquareNormalMode() {

        return new GraphicElement[]{
                new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
                    @Override
                    public int getValue() {
                        return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
                    }

                    @Override
                    public boolean isNeedRedraw() {
                        return mOmniWatchView.isNeedRedraw();
                    }

                    @Override
                    public GraphicElement[] getGraphicElement() {
                        return new GraphicElement[]{
                                mBackground,
                                new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
                                    @Override
                                    public String getText() {
                                        return mOmniWatchView.getCalendar().getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
                                    }


                                    @Override
                                    public float getPercentX() {
                                        return 0.19f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.5f;
                                    }
                                }),
                                createMinuteBackground(getURl("nexo_paski_square_paski")),
                                new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
                                    @Override
                                    public BitmapPart getmBitmapPart() {
                                        return new BitmapPartUrl(getURl("nexo_paski"));
                                    }

                                    @Override
                                    public Paint createPaint() {
                                        return mPaint;
                                    }
                                }),
                                new SimpleTextGraphicElement(new DateTextGraphicDescription() {


                                    @Override
                                    protected int getField() {
                                        return Calendar.DAY_OF_MONTH;
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.19f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.56f;
                                    }
                                }),
                                new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
                                    @Override
                                    public String getText() {
                                        return "Sec";
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.81f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.50f;
                                    }

                                }),
                                new CounterGraphicElement(new MultiBitmapMultiStateGraphicElement.MultiBitmapMultiStateGraphicsDescription() {


                                    @Override
                                    public int getValue() {
                                        return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
                                    }

                                    @Override
                                    public int getSize() {
                                        return 2;
                                    }

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(
                                                getURl("number_0"),
                                                getURl("number_1"),
                                                getURl("number_2"),
                                                getURl("number_3"),
                                                getURl("number_4"),
                                                getURl("number_5"),
                                                getURl("number_6"),
                                                getURl("number_7"),
                                                getURl("number_8"),
                                                getURl("number_9"));

                                    }

                                    @Override
                                    public Paint createPaint() {
                                        return mPaint;
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.5f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.5f;
                                    }
                                }),
                                new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

                                    @Override
                                    public Paint createPaint() {
                                        return null;
                                    }

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(getURl("days"));
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.5f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.29f;
                                    }
                                }),
                                new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
                                    @Override
                                    public float getPercentX() {
                                        return 0.5f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.29f;
                                    }


                                    @Override
                                    public int getValue() {
                                        return mOmniWatchView.getCalendar().get(Calendar.DAY_OF_WEEK) - 1;
                                    }

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(
                                                getURl("day_7"),
                                                getURl("day_1"),
                                                getURl("day_2"),
                                                getURl("day_3"),
                                                getURl("day_4"),
                                                getURl("day_5"),
                                                getURl("day_6"));
                                    }

                                    @Override
                                    public Paint createPaint() {
                                        return mPaint;
                                    }
                                }),
                                new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
                                    @Override
                                    public float getPercentX() {
                                        return 0.5f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.68f;
                                    }


                                    @Override
                                    public int getValue() {
                                        return mOmniWatchView.getCalendar().get(Calendar.HOUR_OF_DAY) > 11 ? 1 : 0;
                                    }

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(
                                                getURl("am"),
                                                getURl("pm"));
                                    }

                                    @Override
                                    public Paint createPaint() {
                                        return null;
                                    }
                                }),
                                new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {


                                    @Override
                                    public Paint createPaint() {
                                        return null;
                                    }

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(getURl("bateria_symbol"));
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.38f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.7f;
                                    }
                                }),
                                new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

                                    @Override
                                    public Paint createPaint() {
                                        return null;
                                    }

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(getURl("b_d"));
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.5f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.73f;
                                    }

                                }),
                                new OneBitmapMultiStateGraphicElement(batteryLevelGraphicDescription),
                                new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(getURl("b_g"));
                                    }

                                    @Override
                                    public Paint createPaint() {
                                        return null;
                                    }

                                    @Override
                                    public float getPercentX() {
                                        return 0.5f;
                                    }

                                    @Override
                                    public float getPercentY() {
                                        return 0.73f;
                                    }
                                }),
                                new CustomDrawableTipsGraphicElement(new CustomDrawableTipsGraphicElement.CustomDrawerTipsDescription() {
                                    @Override
                                    public BitmapPart createBitmapPart() {
                                        return new BitmapPartUrl(
                                                getURl("tip_1"),
                                                getURl("tip_2"));
                                    }

                                    @Override
                                    public float getLocation() {
                                        return 0.84f;
                                    }

                                    @Override
                                    public Paint createPaint() {
                                        return mPaint;
                                    }
                                })
                        };
                    }
                }),
                new SimpleTextGraphicElement(new DateTextGraphicDescription() {

                    @Override
                    protected int getField() {
                        return Calendar.SECOND;
                    }

                    @Override
                    public float getPercentX() {
                        return 0.81f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.56f;
                    }


                }),
        };
    }

    @Override
    public GraphicElement[] createSquareAmbitionMode() {
        return createMinuteAmbition(new SimpleSquareBackgroundGraphicElement(Color.BLACK));
    }

    @Override
    public GraphicElement[] createRoundAmbitionMode() {
        return createMinuteAmbition(new SimpleRoundeBackgroundGraphicElement(Color.BLACK));
    }

    @Override
    public void createPaint() {
        mPaint = new Paint();
        mPaintText = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPaintTextDescription = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Thin.ttf"));
        mPainAmbition = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Thin.ttf"));
    }

    @NonNull
    private BackgroundGraphicElement createBackground(final String url) {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartUrl(url);
            }

            @Override
            public Paint createPaint() {
                return null;
            }
        });
        return mBackground;
    }

    private Paint createTextPaint(int color, Typeface typeface) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setTextSize(10);
        paint.setTypeface(typeface);
        return paint;
    }

    @Override
    public void setParams(Params params) {
        float saturation = ColorParamsAdapter.getSaturation(params);
        float hue = ColorParamsAdapter.getHua(params);
        mPaint.setColorFilter(ColorFilterGenerator.adjustColor(saturation, hue));
        mPaintTextDescription.setColorFilter(ColorFilterGenerator.adjustColor(saturation, hue));
        mPainAmbition.setColorFilter(ColorFilterGenerator.adjustColor(saturation, hue));
    }

    @Override
    public LifeCycleListeners[] createLiveCycleListeners() {
        batteryLevelGraphicDescription = new BatteryLevelGraphicDescription(9) {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartUrl(
                        getURl("b_10"),
                        getURl("b_20"),
                        getURl("b_30"),
                        getURl("b_40"),
                        getURl("b_50"),
                        getURl("b_60"),
                        getURl("b_70"),
                        getURl("b_80"),
                        getURl("b_90"),
                        getURl("b_100"));
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.73f;
            }


        };
        return new LifeCycleListeners[]{batteryLevelGraphicDescription};
    }

    @NonNull
    private GraphicElement[] createMode(final SimpleBitmapGraphicElement minuteBackground) {
        return new GraphicElement[]{
                new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
                    @Override
                    public int getValue() {
                        return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
                    }

                    @Override
                    public boolean isNeedRedraw() {
                        return mOmniWatchView.isNeedRedraw();
                    }

                    @Override
                    public GraphicElement[] getGraphicElement() {
                        return new GraphicElement[]
                                {
                                        mBackground,
                                        new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
                                            @Override
                                            public String getText() {
                                                return mOmniWatchView.getCalendar().getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.getDefault());
                                            }


                                            @Override
                                            public float getPercentX() {
                                                return 0.19f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.5f;
                                            }
                                        }),
                                        minuteBackground,
                                        new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
                                            @Override
                                            public String getText() {
                                                return "Sec";
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.81f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.50f;
                                            }

                                        }),
                                        new SimpleTextGraphicElement(new DateTextGraphicDescription() {

                                            @Override
                                            protected int getField() {
                                                return Calendar.DAY_OF_MONTH;
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.19f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.56f;
                                            }
                                        }),
                                        new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {


                                            @Override
                                            public Paint createPaint() {
                                                return null;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(getURl("days"));
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.29f;
                                            }
                                        }),
                                        new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {


                                            @Override
                                            public Paint createPaint() {
                                                return null;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(getURl("bateria_symbol"));
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.38f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.7f;
                                            }
                                        }),
                                        new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

                                            @Override
                                            public Paint createPaint() {
                                                return null;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(getURl("b_d"));
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.73f;
                                            }

                                        }),
                                        new CounterGraphicElement(new MultiBitmapMultiStateGraphicElement.MultiBitmapMultiStateGraphicsDescription() {


                                            @Override
                                            public int getValue() {
                                                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
                                            }

                                            @Override
                                            public int getSize() {
                                                return 2;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(
                                                        getURl("number_0"),
                                                        getURl("number_1"),
                                                        getURl("number_2"),
                                                        getURl("number_3"),
                                                        getURl("number_4"),
                                                        getURl("number_5"),
                                                        getURl("number_6"),
                                                        getURl("number_7"),
                                                        getURl("number_8"),
                                                        getURl("number_9"));

                                            }

                                            @Override
                                            public Paint createPaint() {
                                                return mPaint;
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.5f;
                                            }
                                        }),
                                        new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.29f;
                                            }


                                            @Override
                                            public int getValue() {
                                                return mOmniWatchView.getCalendar().get(Calendar.DAY_OF_WEEK) - 1;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(
                                                        getURl("day_7"),
                                                        getURl("day_1"),
                                                        getURl("day_2"),
                                                        getURl("day_3"),
                                                        getURl("day_4"),
                                                        getURl("day_5"),
                                                        getURl("day_6"));
                                            }

                                            @Override
                                            public Paint createPaint() {
                                                return mPaint;
                                            }
                                        }),
                                        new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.68f;
                                            }


                                            @Override
                                            public int getValue() {
                                                return mOmniWatchView.getCalendar().get(Calendar.HOUR_OF_DAY) > 11 ? 1 : 0;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(
                                                        getURl("am"),
                                                        getURl("pm"));
                                            }

                                            @Override
                                            public Paint createPaint() {
                                                return null;
                                            }
                                        }),
                                        new OneBitmapMultiStateGraphicElement(batteryLevelGraphicDescription),
                                        new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

                                            @Override
                                            public Paint createPaint() {
                                                return null;
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(
                                                        getURl("b_g"));
                                            }

                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.73f;
                                            }
                                        }),
                                        new CustomDrawableTipsGraphicElement(new CustomDrawableTipsGraphicElement.CustomDrawerTipsDescription() {
                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(
                                                        getURl("tip_1"),
                                                        getURl("tip_2"));
                                            }

                                            @Override
                                            public float getLocation() {
                                                return 0.84f;
                                            }

                                            @Override
                                            public Paint createPaint() {
                                                return mPaint;
                                            }
                                        })
                                };

                    }
                }),
                new SimpleTextGraphicElement(new DateTextGraphicDescription() {

                    @Override
                    protected int getField() {
                        return Calendar.SECOND;
                    }

                    @Override
                    public float getPercentX() {
                        return 0.81f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.56f;
                    }


                })};
    }

    @NonNull
    private GraphicElement[] createMinuteAmbition(final GraphicElement background) {
        return new GraphicElement[]{
                new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
                    @Override
                    public int getValue() {
                        return mOmniWatchView.getCalendar().get(Calendar.HOUR_OF_DAY);
                    }

                    @Override
                    public boolean isNeedRedraw() {
                        return mOmniWatchView.isNeedRedraw() ;
                    }

                    @Override
                    public GraphicElement[] getGraphicElement() {
                        return new GraphicElement[]
                                {
                                        background,
                                        new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
                                            @Override
                                            public float getPercentX() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public float getPercentY() {
                                                return 0.5f;
                                            }

                                            @Override
                                            public int getValue() {
                                                return mOmniWatchView.getCalendar().get(Calendar.HOUR);
                                            }

                                            @Override
                                            public BitmapPart createBitmapPart() {
                                                return new BitmapPartUrl(
                                                        getURl("h_12"),
                                                        getURl("h_13"),
                                                        getURl("h_14"),
                                                        getURl("h_15"),
                                                        getURl("h_16"),
                                                        getURl("h_17"),
                                                        getURl("h_18"),
                                                        getURl("h_19"),
                                                        getURl("h_20"),
                                                        getURl("h_21"),
                                                        getURl("h_22"),
                                                        getURl("h_23"));


                                            }

                                            @Override
                                            public Paint createPaint() {
                                                return mPaint;
                                            }
                                        }
                                        )}

                                ;
                    }
                }),
                new SimpleTextGraphicElement(new DateTextGraphicDescription() {

                    @Override
                    protected int getField() {
                        return Calendar.MINUTE;
                    }

                    @Override
                    public Paint getPaint() {
                        return mPainAmbition;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.main_text_in_ambition);
                    }

                    @Override
                    public float getPercentX() {
                        return 0.5f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.5f;
                    }


                })
        };
    }

    @NonNull
    private SimpleBitmapGraphicElement createMinuteBackground(final String backgroundMinute) {
        return new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {


            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartUrl(backgroundMinute);
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.5f;
            }
        });
    }

    @NonNull
    private String getDataText(int field) {
        int seconds = mOmniWatchView.getCalendar().get(field);
        String value = String.valueOf(seconds);
        return seconds < 10 ? "0" + value : value;
    }

    abstract class SimpleTextGraphicDescription implements TextGraphicDescription {

        @Override
        public Paint getPaint() {
            return mPaintText;
        }

        @Override
        public float getTextSize() {
            return mOmniWatchView.getSizeFromResources(R.dimen.text_simple);
        }
    }

    abstract class DateTextGraphicDescription implements TextGraphicDescription {
        @Override
        public Paint getPaint() {
            return mPaintTextDescription;
        }

        @Override
        public float getTextSize() {
            return mOmniWatchView.getSizeFromResources(R.dimen.text_size);
        }


        @Override
        public String getText() {
            return getDataText(getField());
        }

        protected abstract int getField();
    }
}
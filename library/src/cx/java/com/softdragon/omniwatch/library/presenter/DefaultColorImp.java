package com.softdragon.omniwatch.library.presenter;

/**
 * Created by Dawid on 2016-07-12.
 */
public class DefaultColorImp implements DefaultColor {
    @Override
    public float getDefaultHua() {
        return 145f;
    }

    @Override
    public float getDefaultSaturation() {
        return 100f;
    }

}

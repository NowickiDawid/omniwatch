package com.softdragon.omniwatch.library.presenter;

import android.graphics.ColorFilter;
import android.graphics.Paint;

import com.softdragon.omniwatch.library.R;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.utils.TextUtils;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.RotatingBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.SimpleBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleRoundeBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleSquareBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.imp.BackgroundGraphicDescriptionImp;
import com.softdragon.omniwatch.library.view.element.battery.BatteryLevelGraphicDescription;
import com.softdragon.omniwatch.library.view.element.clock.ClockBitmapGraphicDescription;
import com.softdragon.omniwatch.library.view.element.clock.ClockBitmapGraphicSetter;
import com.softdragon.omniwatch.library.view.element.group.BitmapGroupElement;
import com.softdragon.omniwatch.library.view.element.text.SimpleTextGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPartResource;
import com.softdragon.omniwatch.library.view.element.utils.ColorFilterGenerator;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dawid on 2016-02-11.
 */
public class WatchFace extends WatchFaceBaseImp {
    private static final float CENTER_X = 0.50f;
    private static final float CENTER_Y = 0.50f;
    private static final float BATTERY_X = 0.38f;
    private static final float CENTER_OF_CHINK_Y = CENTER_Y + 0.07f;
    private static final float BATTERY_Y = 0.44f;
    private static final float CENTER_OF_CHINK_SECONDS_Y = CENTER_Y + 0.109f;
    private static final float CENTER_OF_CHINK_SECONDS_X = 0.498f;
    private static final float TIME_X = 0.50f;
    private static final float TIME_SECONDS = 0.67f;
    private static final float TIME_PM_AM_X = 0.33f;

    private Paint mPaintTextBigger;
    private BackgroundGraphicElement mBackground;
    private Paint mPaint;
    private Paint mPaintTextSmall;
    private RotateBatteryLevelGraphicDescription mRotateBatteryLevelGraphicDescription;
    private RotateBatteryLevelGraphicDescription mRotateColorBatteryLevelGraphicDescription;
    private Paint mPaintBigText;
    private Paint mPaintSmall;
    private Paint mPaintKmBig;
    private Paint mPaintKmSmall;
    private float mValue = 0.13f;
    private float mPart = 100f;
    private Paint mPaintAMPM;
    private WatchParamAdapterImp mWatchParamsAdapterImp;

    public WatchFace(OmniWatchView omniWatchView) {
        super(omniWatchView);
    }

    @Override
    public void createPaint() {
        mPaintTextBigger = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaintTextSmall = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaintBigText = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaintSmall = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaintAMPM = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaintKmBig = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaintKmSmall = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/play.ttf"));
        mPaint = mOmniWatchView.createStandardPaint();
        mWatchParamsAdapterImp = new WatchParamAdapterImp(mOmniWatchView.getContext());


    }

    @Override
    public BackgroundGraphicElement createRoundBackground() {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicDescriptionImp() {

            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.background);
            }
        });
        return mBackground;
    }

    @Override
    public BackgroundGraphicElement createSquareBackground() {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicDescriptionImp() {

            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.square_background);
            }
        });
        return mBackground;
    }

    @Override
    public List<GraphicElement> createRoundNormalMode() {
        return createModeGraphics(new BitmapPartResource(R.mipmap.background_color), new BitmapPartResource(R.mipmap.background_black));

    }

    @Override
    public List<GraphicElement> createRoundAmbitionMode() {
        ArrayList<GraphicElement> list = new ArrayList<>();
        list.add(new SimpleRoundeBackgroundGraphicElement(mOmniWatchView.getResources().getColor(android.R.color.black)));
        addAmbientMode(list);
        return list;
    }

    private void addAmbientMode(List<GraphicElement> list) {
        final ClockBitmapGraphicDescription clockBitmapGraphicDescription = createNotClockBitmap();
        list.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.ambient_index);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
        addTimeText(list);
        addMinuteAndHours(clockBitmapGraphicDescription, list);
        addScrew(list);

    }

    @Override
    public List<GraphicElement> createSquareNormalMode() {
        return createModeGraphics(new BitmapPartResource(R.mipmap.background_color), new BitmapPartResource(R.mipmap.background_black));

    }

    private ArrayList<GraphicElement> createModeGraphics(final BitmapPart colorGraphics, final BitmapPart shadows) {
        ArrayList<GraphicElement> list = new ArrayList<>();
        final ClockBitmapGraphicDescription clockColorBitmapGraphicDescription = createColorClockBitmap();
        final ClockBitmapGraphicDescription clockBitmapGraphicDescription = createNotClockBitmap();

        list.add(new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
            }

            @Override
            public boolean isNeedRedraw() {
                return mOmniWatchView.isNeedRedraw();
            }

            @Override
            public List<GraphicElement> getGraphicElement() {
                List<GraphicElement> listGraphicElement = new ArrayList<>();
                listGraphicElement.add(mBackground);
                listGraphicElement.add(new BackgroundGraphicElement(new BackgroundGraphicDescriptionImp() {
                    @Override
                    public BitmapPart getmBitmapPart() {
                        return shadows;
                    }
                }));
                listGraphicElement.add(new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
                    @Override
                    public BitmapPart getmBitmapPart() {
                        return colorGraphics;
                    }

                    @Override
                    public Paint createPaint() {
                        return mPaint;
                    }
                }));
                listGraphicElement.add(new SimpleTextGraphicElement(new TimeDateTextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintTextBigger;
                    }

                    @Override
                    protected int getField() {
                        return Calendar.DAY_OF_MONTH;
                    }

                    @Override
                    public float getPercentX() {
                        return 0.5f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.24f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_day_of_month);
                    }


                }));
                listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintTextSmall;
                    }

                    @Override
                    public String getText() {
                        return TextUtils.getCaptionText(mOmniWatchView.getCalendar().getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH));
                    }

                    @Override
                    public float getPercentX() {
                        return 0.5f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.295f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_month);
                    }


                }));
                addTimeText(listGraphicElement);

                listGraphicElement.add(new RotatingBitmapGraphicElement(mRotateBatteryLevelGraphicDescription));
                listGraphicElement.add(new RotatingBitmapGraphicElement(mRotateColorBatteryLevelGraphicDescription));

                listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintKmSmall;
                    }

                    @Override
                    public String getText() {
                        mValue += mPart;
                        if (mValue > 10000)
                            return "";

                        int smallPart = (int) ((mValue - (int) mValue) * 100);
                        if (mValue >= 1000)
                            return "" + smallPart / 10;
                        else
                            return getTimeToText(smallPart);
                    }

                    @Override
                    public float getPercentX() {
                        return mValue < 1000 ? 0.77f : 0.785f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.375f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_km_small);
                    }


                }));
                listGraphicElement.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
                    @Override
                    public BitmapPart createBitmapPart() {
                        return new BitmapPartResource(R.mipmap.bike);
                    }

                    @Override
                    public Paint createPaint() {
                        return mPaint;
                    }

                    @Override
                    public float getPercentX() {
                        return 0.635f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.28f;
                    }
                }));
                listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintKmSmall;
                    }

                    @Override
                    public String getText() {
                        return "KM";
                    }


                    @Override
                    public float getPercentX() {
                        return 0.725f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.30f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_km_small);
                    }


                }));
                listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintKmBig;
                    }

                    @Override
                    public String getText() {
                        String value = getTimeToHoundrets((int) mValue);
                        if (mValue < 10000)
                            return value + ",";
                        return ((int) mValue) + "";
                    }

                    @Override
                    public float getPercentX() {
                        if (mValue < 1000)
                            return 0.665f;
                        if (mValue < 10000)
                            return 0.675f;
                        return 0.69f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.37f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_km_big);
                    }


                }));
                return listGraphicElement;
            }
        }));
        list.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
            @Override
            public Paint getPaint() {
                return mPaintSmall;
            }

            @Override
            public String getText() {
                return getTimeToText(mOmniWatchView.getCalendar().get(Calendar.SECOND));
            }

            @Override
            public float getPercentX() {
                return TIME_SECONDS;
            }

            @Override
            public float getPercentY() {
                return 0.685f;
            }

            @Override
            public float getTextSize() {
                return mOmniWatchView.getSizeFromResources(R.dimen.text_time_seconds);
            }


        }));
        addMinuteAndHours(clockBitmapGraphicDescription, list);
        list.add(ClockBitmapGraphicSetter.createSecondGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.createSecondGraphicElement(clockColorBitmapGraphicDescription));
        addScrew(list);
        return list;
    }

    private void addScrew(List<GraphicElement> list) {
        list.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.screw);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
    }

    private void addTimeText(List<GraphicElement> listGraphicElement) {
        listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
            @Override
            public Paint getPaint() {
                return mPaintAMPM;
            }

            @Override
            public String getText() {
                return mOmniWatchView.is12Hour() ? (mOmniWatchView.getCalendar().get(Calendar.AM_PM) == 0 ? "AM" : "PM") : "";
            }

            @Override
            public float getPercentX() {
                return TIME_PM_AM_X;
            }

            @Override
            public float getPercentY() {
                return 0.69f;
            }

            @Override
            public float getTextSize() {
                return mOmniWatchView.getSizeFromResources(R.dimen.text_am_pm);
            }


        }));
        listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
            @Override
            public Paint getPaint() {
                return mPaintBigText;
            }

            @Override
            public String getText() {
                int hour = mOmniWatchView.getCalendar().get(Calendar.HOUR);
                String hourText = getTimeToText(mOmniWatchView.is12Hour() ? hour == 0 ? 12 : hour : mOmniWatchView.getCalendar().get(Calendar.HOUR_OF_DAY));
                return mOmniWatchView.getResources().getString(R.string.time, hourText, getTimeToText(mOmniWatchView.getCalendar().get(Calendar.MINUTE)));
            }

            @Override
            public float getPercentX() {
                return TIME_X;
            }

            @Override
            public float getPercentY() {
                return 0.68f;
            }

            @Override
            public float getTextSize() {
                return mOmniWatchView.getSizeFromResources(R.dimen.text_time);
            }


        }));
    }

    private void addMinuteAndHours(ClockBitmapGraphicDescription clockBitmapGraphicDescription, List<GraphicElement> list) {
        list.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockBitmapGraphicDescription));
    }

    private ClockBitmapGraphicDescription createNotClockBitmap() {
        return new ClockBitmapGraphicDescriptionImpl() {

            @Override
            public BitmapPart createHourBitmapPart() {
                return new BitmapPartResource(R.mipmap.hours);
            }

            @Override
            public Paint getClockPaint() {
                return null;
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return new BitmapPartResource(R.mipmap.minutes);
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return new BitmapPartResource(R.mipmap.seconds);
            }
        };
    }

    private ClockBitmapGraphicDescription createColorClockBitmap() {
        return new ClockBitmapGraphicDescriptionImpl() {

            @Override
            public BitmapPart createHourBitmapPart() {
                return null;
            }

            @Override
            public Paint getClockPaint() {
                return mPaint;
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return null;
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return new BitmapPartResource(R.mipmap.seconds_color);
            }
        };
    }

    @Override
    public List<GraphicElement> createSquareAmbitionMode() {
        List<GraphicElement> list = new ArrayList<>();
        list.add(new SimpleSquareBackgroundGraphicElement(mOmniWatchView.getResources().getColor(android.R.color.black)));
        addAmbientMode(list);

        return list;
    }


    @Override
    public List<LifeCycleListeners> createLiveCycleListeners() {
        mRotateBatteryLevelGraphicDescription = createBatteryLevelGraphicDescription(R.mipmap.battery, null);
        mRotateColorBatteryLevelGraphicDescription = createBatteryLevelGraphicDescription(R.mipmap.battery_color, mPaint);
        List<LifeCycleListeners> list = new ArrayList<>();
        list.add(mRotateBatteryLevelGraphicDescription);
        list.add(mRotateColorBatteryLevelGraphicDescription);
        return list;
    }

    @Override
    public void setParams(Params params) {
        ColorFilter filter = getColorFilter(params, mWatchParamsAdapterImp.getAdapter());
        mPaintBigText.setColorFilter(filter);
        mPaint.setColorFilter(filter);
        mPaintSmall.setColorFilter(filter);
        mPaintKmSmall.setColorFilter(filter);
        mPaintKmBig.setColorFilter(filter);
        mOmniWatchView.set12Hour(mWatchParamsAdapterImp.getAdapter().get12Hour(params));
        mPaintAMPM.setColorFilter(filter);

    }

    private RotateBatteryLevelGraphicDescription createBatteryLevelGraphicDescription(final int batteryRes, final Paint paint) {
        return new RotateBatteryLevelGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(batteryRes);
            }

            @Override
            public Paint createPaint() {
                return paint;
            }
        };
    }

    abstract class TimeDateTextGraphicDescription extends DateTextGraphicDescription {

        @Override
        public String getText() {
            return getDataText(getField());
        }

        protected abstract int getField();

    }

    abstract class DateTextGraphicDescription implements TextGraphicDescription {

        @Override
        public float getTextSize() {
            return 10;
        }


    }

    abstract class RotateBatteryLevelGraphicDescription extends BatteryLevelGraphicDescription implements RotatingBitmapGraphicElement.RotatingBitmapGraphicDescription {

        public RotateBatteryLevelGraphicDescription() {
            super(6);
        }

        @Override
        public int getMaxDegree() {
            return 388;
        }

        @Override
        public int getMinDegree() {
            return 260;
        }

        @Override
        public int getMaxValue() {
            return 7;
        }

        @Override
        public int getMinValue() {
            return 0;
        }

        @Override
        public float getRotationY() {
            return BATTERY_Y - 0.056f;
        }

        @Override
        public float getRotationX() {
            return BATTERY_X;
        }


        @Override
        public float getPercentX() {
            return BATTERY_X;
        }

        @Override
        public float getPercentY() {
            return BATTERY_Y;
        }

    }

    abstract class ClockBitmapGraphicDescriptionImpl extends ClockBitmapGraphicDescription {

        @Override
        public Calendar getCalendar() {
            return mOmniWatchView.getCalendar();
        }

        @Override
        public float getPercentChinY() {
            return CENTER_OF_CHINK_Y;
        }

        @Override
        public float getPercentSecondChinX() {
            return CENTER_OF_CHINK_SECONDS_X;
        }

        @Override
        public float getPercentSecondChinY() {
            return CENTER_OF_CHINK_SECONDS_Y;
        }

        @Override
        public float getPercentX() {
            return CENTER_X;
        }

        @Override
        public float getPercentY() {
            return CENTER_Y;
        }
    }
}
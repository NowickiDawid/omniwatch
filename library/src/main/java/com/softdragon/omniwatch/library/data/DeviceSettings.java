package com.softdragon.omniwatch.library.data;

import android.os.Bundle;

import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-03-12.
 */
public class DeviceSettings {
    Bundle data = new Bundle();

    public Bundle getData() {
        return data;
    }

    public void setData(Bundle data) {
        this.data = data;
    }


}

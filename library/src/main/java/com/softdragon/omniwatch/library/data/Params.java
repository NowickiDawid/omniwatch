package com.softdragon.omniwatch.library.data;

import android.os.Bundle;

/**
 * Created by Dawid on 2016-03-12.
 */
public class Params {
    Bundle data = new Bundle();

    public Bundle getData() {
        return data;
    }

    public void setData(Bundle data) {
        this.data = data;
    }
}

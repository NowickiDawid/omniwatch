package com.softdragon.omniwatch.library.data;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by Dawid on 2016-02-18.
 */
public class SendDataByDataLayerThread extends Thread {
    public final String TAG = "SendMessageByDataLayerThread";
    private final GoogleApiClient client;
    String path;
    DataMap dataMap;

    public SendDataByDataLayerThread(String path, DataMap dataMap, GoogleApiClient client) {
        this.path = path;
        this.dataMap = dataMap;
        this.client = client;
    }

    public void run() {
        // Construct a DataRequest and send over the data layer
        PutDataMapRequest putDMR = PutDataMapRequest.create(path);
        putDMR.getDataMap().putAll(dataMap);
        PutDataRequest request = putDMR.asPutDataRequest();
        request.setUrgent();
        DataApi.DataItemResult result = Wearable.DataApi.putDataItem(client, request).await();
        if (result.getStatus().isSuccess()) {
            OmniLog.i(TAG, "DataMap: " + dataMap + " sent successfully to data layer ");
        } else {
            OmniLog.i(TAG, "ERROR: failed to send DataMap to data layer");
        }
    }
}

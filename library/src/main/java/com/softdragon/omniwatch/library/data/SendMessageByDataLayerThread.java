package com.softdragon.omniwatch.library.data;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

/**
 * Created by Dawid on 2016-02-17.
 */
public class SendMessageByDataLayerThread extends Thread {
    public final String TAG = "SendMessageByDataLayerThread";
    private final GoogleApiClient client;
    String path;
    String message;

    // Constructor to send a message to the data layer
    public SendMessageByDataLayerThread(String path, String msg, GoogleApiClient client) {
        this.path = path;
        this.message = msg;
        this.client = client;
    }

    @Override
    public void run() {
        NodeApi.GetConnectedNodesResult nodes = Wearable.NodeApi.getConnectedNodes(client).await();
        sendMessageToNodes(nodes);

    }

    private void sendMessageToNodes(NodeApi.GetConnectedNodesResult nodes) {
        for (Node node : nodes.getNodes()) {
            MessageApi.SendMessageResult result = Wearable.MessageApi.sendMessage(client, node.getId(), path, message.getBytes()).await();
            if (result.getStatus().isSuccess()) {
                OmniLog.i(TAG, "Message: {" + message + "} sent to: " + node.getDisplayName());
            } else {
                OmniLog.i(TAG, "ERROR: failed to send Message");
            }
        }
    }
}

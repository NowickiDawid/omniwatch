package com.softdragon.omniwatch.library.presenter;

/**
 * Created by Dawid on 2016-03-14.
 */
public interface DefaultColor {
    float getDefaultHua();

    float getDefaultSaturation();

}

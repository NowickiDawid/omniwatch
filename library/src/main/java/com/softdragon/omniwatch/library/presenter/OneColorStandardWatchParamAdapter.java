package com.softdragon.omniwatch.library.presenter;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.wearable.DataMap;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.presenter.params.WatchParamAdapter;
import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-07-11.
 */
public class OneColorStandardWatchParamAdapter extends WatchParamAdapter {
    private ColorParamsAdapter mGlobal;

    public OneColorStandardWatchParamAdapter(Context context) {
        mGlobal = new ColorParamsAdapter(context);
    }

    @Override
    public ColorParamsAdapter getAdapter() {
        return mGlobal;
    }

    @Override
    public ColorParamsAdapter getColorParamsAdapter(int index) {
        return mGlobal;
    }


    @Override
    public void save(DataMap dataMap, StoragePreference storagePreference) {
        mGlobal.save(dataMap, storagePreference);
    }

    @Override
    public void save(Params dataMap, StoragePreference storagePreference) {
        mGlobal.save(dataMap, storagePreference);
    }

    @Override
    public Params load(StoragePreference storage) {
        return mGlobal.load(storage);
    }

    @Override
    public DataMap getData(Params params) {
        return mGlobal.getData(params);
    }
}

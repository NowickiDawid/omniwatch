package com.softdragon.omniwatch.library.presenter;

import android.os.Bundle;

import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

import java.util.List;

/**
 * Created by Dawid on 2016-02-12.
 */
public interface WatchFaceBase {
    void createPaint();

    BackgroundGraphicElement createRoundBackground();
    BackgroundGraphicElement createSquareBackground();

    List<GraphicElement> createRoundNormalMode();
    List<GraphicElement> createRoundAmbitionMode();

    List<GraphicElement> createSquareNormalMode();
    List<GraphicElement> createSquareAmbitionMode();

    List<LifeCycleListeners> createLiveCycleListeners();


    void setParams(Params params);
}

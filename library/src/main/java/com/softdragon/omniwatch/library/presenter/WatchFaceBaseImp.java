package com.softdragon.omniwatch.library.presenter;

import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.utils.ColorFilterGenerator;

/**
 * Created by Dawid on 2016-06-20.
 */
public abstract class WatchFaceBaseImp implements WatchFaceBase {
    protected final OmniWatchView mOmniWatchView;

    public WatchFaceBaseImp(OmniWatchView omniWatchView) {
        mOmniWatchView = omniWatchView;
    }

    protected int getColor(int colorRes) {
        return mOmniWatchView.getContext().getResources().getColor(colorRes);
    }

    protected Paint createTextPaint(int color, Typeface typeface) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(color);
        paint.setTypeface(typeface);
        return paint;
    }

    protected void setColors(Paint paint, Params params, ColorParamsAdapter colorParamsAdapter) {
        ColorFilter filter = getColorFilter(params, colorParamsAdapter);
        paint.setColorFilter(filter);
    }

    protected ColorFilter getColorFilter(Params params, ColorParamsAdapter colorParamsAdapter) {
        float saturation = colorParamsAdapter.getSaturation(params);
        float hue = colorParamsAdapter.getHua(params);
        return ColorFilterGenerator.adjustColor(saturation, hue);
    }

    @NonNull
    protected String getDataText(int field) {
        int seconds = mOmniWatchView.getCalendar().get(field);
        return getTimeToText(seconds);
    }

    @NonNull
    protected String getTimeToText(int seconds) {
        String value = String.valueOf(seconds);
        return seconds < 10 ? "0" + value : value;
    }
    @NonNull
    protected String getTimeToHoundrets(int seconds) {
        String value = String.valueOf(seconds);
        if(seconds<10)
            return "00"+value;
        else if(seconds<100)
            return "0"+value;
        return value;
    }
}

package com.softdragon.omniwatch.library.presenter;

import android.os.Bundle;

import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

/**
 * Created by Dawid on 2016-02-11.
 */
public class WatchPresenter {
    public WatchFaceBase mWatchFace;

    public WatchPresenter(OmniWatchView omniWatchView) {
        mWatchFace = new WatchFace(omniWatchView);
    }

    public void createPaint() {
        mWatchFace.createPaint();
    }

    public BackgroundGraphicElement createRoundBackground() {
        return mWatchFace.createRoundBackground();
    }

    public BackgroundGraphicElement createSquareBackground() {
        return mWatchFace.createSquareBackground();
    }

    public GraphicElement[] createRoundAmbitionMode() {
        return mWatchFace.createRoundAmbitionMode().toArray(new GraphicElement[0]);
    }

    public GraphicElement[] createSquareNormalMode() {
        return mWatchFace.createSquareNormalMode().toArray(new GraphicElement[0]);
    }
    public GraphicElement[] createSquareAmbitionMode() {
        return mWatchFace.createSquareAmbitionMode().toArray(new GraphicElement[0]);
    }

    public GraphicElement[] createRoundNormalMode() {
        return mWatchFace.createRoundNormalMode().toArray(new GraphicElement[0]);
    }


    public LifeCycleListeners[] createLiveCycleListeners() {
        return mWatchFace.createLiveCycleListeners().toArray(new LifeCycleListeners[0]);
    }


    public void setParameters(Params params) {
        mWatchFace.setParams(params);
    }
}

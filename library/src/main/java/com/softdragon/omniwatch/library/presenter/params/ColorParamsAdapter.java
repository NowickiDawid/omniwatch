package com.softdragon.omniwatch.library.presenter.params;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.format.DateFormat;

import com.google.android.gms.wearable.DataMap;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.DefaultColor;
import com.softdragon.omniwatch.library.presenter.DefaultColorImp;
import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-02-13.
 */
public class ColorParamsAdapter {

    private DefaultColor mDefualtColor;
    private String mSaturationKeyParam = "saturation";
    private String mHuaKeyParam = "hua";
    private String mHours12HourParams = "12_hours";
    private String mBlackModeParams = "black_mode";
    private static boolean DEFAULT_HOURS_12_PARAMS = false;
    private static boolean DEFAULT_BLACK_MODE_PARAMS = false;
    private boolean mIs12Able = false;
    private boolean mBlackAble;

    public ColorParamsAdapter(Context context) {
        mDefualtColor = new DefaultColorImp();
        init(context);
    }

    private void init(Context context) {
        DEFAULT_HOURS_12_PARAMS = !DateFormat.is24HourFormat(context);
    }

    public ColorParamsAdapter(Context context, String key, DefaultColor defaultColor) {
        key = key + "_";
        mDefualtColor = defaultColor;
        mSaturationKeyParam = key + mSaturationKeyParam;
        mHuaKeyParam = key + mHuaKeyParam;
        mHours12HourParams = key + mHours12HourParams;
        mBlackModeParams = key + mBlackModeParams;
        init(context);
    }

    public float getSaturation(Params params) {
        Bundle data = params.getData();
        return data.getFloat(mSaturationKeyParam);
    }

    public float getHua(Params params) {
        Bundle data = params.getData();
        return data.getFloat(mHuaKeyParam);
    }

    public boolean get12Hour(Params params) {
        Bundle data = params.getData();
        return data.getBoolean(mHours12HourParams);
    }

    public boolean getBlackMode(Params params) {
        Bundle data = params.getData();
        return data.getBoolean(mBlackModeParams);
    }

    public void set12Hour(boolean is12hour, Params params) {
        Bundle data = params.getData();
        data.putBoolean(mHours12HourParams, is12hour);
    }

    public void setBlackMode(boolean is12hour, Params params) {
        Bundle data = params.getData();
        data.putBoolean(mBlackModeParams, is12hour);
    }

    public void setSaturation(float saturation, Params params) {
        Bundle data = params.getData();
        data.putFloat(mSaturationKeyParam, saturation);
    }

    public void setHua(float hua, Params params) {
        Bundle data = params.getData();
        data.putFloat(mHuaKeyParam, hua);
    }


    public DataMap getData(Params params) {
        DataMap maps = new DataMap();
        getData(params, maps);
        return maps;

    }

    public void getData(Params params, DataMap maps) {
        Bundle data = params.getData();
        maps.putFloat(mSaturationKeyParam, data.getFloat(mSaturationKeyParam));
        maps.putFloat(mHuaKeyParam, data.getFloat(mHuaKeyParam));
        maps.putBoolean(mHours12HourParams, data.getBoolean(mHours12HourParams));
        maps.putBoolean(mBlackModeParams, data.getBoolean(mBlackModeParams));
    }

    public Params load(StoragePreference preference) {
        Bundle data = new Bundle();
        load(preference, data);
        return createParams(data);
    }

    public void load(StoragePreference preference, Bundle data) {
        data.putFloat(mSaturationKeyParam, preference.getFloat(mSaturationKeyParam, mDefualtColor.getDefaultSaturation()));
        data.putFloat(mHuaKeyParam, preference.getFloat(mHuaKeyParam, mDefualtColor.getDefaultHua()));
        data.putBoolean(mHours12HourParams, preference.getBoolean(mHours12HourParams, DEFAULT_HOURS_12_PARAMS));
        data.putBoolean(mBlackModeParams, preference.getBoolean(mBlackModeParams, DEFAULT_BLACK_MODE_PARAMS));
    }

    public Params load(DataMap preference) {
        Bundle data = new Bundle();
        load(preference, data);
        return createParams(data);
    }

    public void load(DataMap preference, Bundle data) {
        data.putFloat(mSaturationKeyParam, preference.getFloat(mSaturationKeyParam, mDefualtColor.getDefaultSaturation()));
        data.putFloat(mHuaKeyParam, preference.getFloat(mHuaKeyParam, mDefualtColor.getDefaultHua()));
        data.putBoolean(mHours12HourParams, preference.getBoolean(mHours12HourParams, DEFAULT_HOURS_12_PARAMS));
        data.putBoolean(mBlackModeParams, preference.getBoolean(mBlackModeParams, DEFAULT_BLACK_MODE_PARAMS));
    }

    @NonNull
    public Params createParams(Bundle data) {
        Params params = new Params();
        params.setData(data);
        return params;
    }

    public void save(DataMap params, StoragePreference preference) {
        preference.putFloat(mSaturationKeyParam, params.getFloat(mSaturationKeyParam));
        preference.putFloat(mHuaKeyParam, params.getFloat(mHuaKeyParam));
        preference.putBoolean(mHours12HourParams, params.getBoolean(mHours12HourParams));
        preference.putBoolean(mBlackModeParams, params.getBoolean(mBlackModeParams));
    }

    public void save(Params params, StoragePreference preference) {
        Bundle data = params.getData();
        preference.putFloat(mSaturationKeyParam, data.getFloat(mSaturationKeyParam));
        preference.putFloat(mHuaKeyParam, data.getFloat(mHuaKeyParam));
        preference.putBoolean(mHours12HourParams, data.getBoolean(mHours12HourParams));
        preference.putBoolean(mBlackModeParams, data.getBoolean(mBlackModeParams));
    }

    public boolean is12Able() {
        return mIs12Able;
    }

    public void setIs12Able(boolean is12Able) {
        mIs12Able = is12Able;
    }

    public void setIsBlackAble(boolean isBlackAble) {
        mBlackAble = isBlackAble;
    }

    public boolean isBlackAble() {
        return mBlackAble;
    }
}

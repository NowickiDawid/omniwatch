package com.softdragon.omniwatch.library.presenter.params;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.wearable.DataMap;
import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-03-14.
 */
public class DeviceSettingsAdapter {
    private static final boolean DEFAULT_VALUE = true;
    private static final String IS_ROUND = "is_round";

    public static boolean isRoundDevice(DeviceSettings deviceSettings) {
        Bundle data = deviceSettings.getData();
        return data.getBoolean(IS_ROUND, DEFAULT_VALUE);
    }

    public static DataMap getData(DeviceSettings params) {
        Bundle data = params.getData();
        DataMap maps = new DataMap();
        maps.putBoolean(IS_ROUND, data.getBoolean(IS_ROUND));
        return maps;

    }

    public static DeviceSettings load(StoragePreference preference) {
        Bundle data = new Bundle();
        data.putBoolean(IS_ROUND, preference.getBoolean(IS_ROUND, DEFAULT_VALUE));
        return createDeviceSettings(data);
    }

    public static DeviceSettings load(DataMap preference) {
        Bundle data = new Bundle();
        data.putBoolean(IS_ROUND, preference.getBoolean(IS_ROUND, DEFAULT_VALUE));
        return createDeviceSettings(data);
    }

    @NonNull
    private static DeviceSettings createDeviceSettings(Bundle data) {
        DeviceSettings deviceSettings = new DeviceSettings();
        deviceSettings.setData(data);
        return deviceSettings;
    }

    public static void save(DataMap params, StoragePreference preference) {
        preference.putBoolean(IS_ROUND, params.getBoolean(IS_ROUND, DEFAULT_VALUE));
    }

    public static void save(DeviceSettings params, StoragePreference preference) {
        Bundle data = params.getData();
        preference.putBoolean(IS_ROUND, data.getBoolean(IS_ROUND, DEFAULT_VALUE));

    }

    public static void setIsDeviceRound(boolean round, DeviceSettings settings) {
        Bundle data = settings.getData();
        data.putBoolean(IS_ROUND, round);
    }
}

package com.softdragon.omniwatch.library.presenter.params;

import com.google.android.gms.wearable.DataMap;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-07-11.
 */
public abstract class WatchParamAdapter {

    public abstract void save(DataMap dataMap, StoragePreference storagePreference);

    public abstract void save(Params dataMap, StoragePreference storagePreference);

    public abstract Params load(StoragePreference storage);

    public abstract DataMap getData(Params params);

    public abstract ColorParamsAdapter getAdapter();

    public abstract ColorParamsAdapter getColorParamsAdapter(int index);
}

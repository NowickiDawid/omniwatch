package com.softdragon.omniwatch.library.service;

import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dawid on 2016-03-10.
 */
public class OmniWatchDataService extends WearableListenerService {
    private static final String TAG = "OmniWatchDataService";
    private OmniWatchInter mOmniWatchInter = new OmniWatchInter();

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        super.onDataChanged(dataEvents);
        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        List<DataEvent> events = getEvents(dataEvents);
        if (!isConnectedSucces(googleApiClient))
            return;
        getDateFromEvents(events);

    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        super.onMessageReceived(messageEvent);
    }

    private void getDateFromEvents(List<DataEvent> events) {
        for (DataEvent event : events) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                mOmniWatchInter.interpretation(event, getBaseContext());
            }
        }
    }


    private boolean isConnectedSucces(GoogleApiClient googleApiClient) {
        ConnectionResult connectionResult =
                googleApiClient.blockingConnect(30, TimeUnit.SECONDS);
        return connectionResult.isSuccess();
    }

    private List<DataEvent> getEvents(DataEventBuffer dataEvents) {
        return FreezableUtils
                .freezeIterable(dataEvents);
    }

}

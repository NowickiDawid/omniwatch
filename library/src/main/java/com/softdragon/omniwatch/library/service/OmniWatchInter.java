package com.softdragon.omniwatch.library.service;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.DeviceSettingsAdapter;
import com.softdragon.omniwatch.library.storage.StoragePreference;
import com.softdragon.omniwatch.library.sychronize.wear.SynDataUtils;

/**
 * Created by Dawid on 2016-03-14.
 */
public class OmniWatchInter {


    public void interpretation(DataEvent event, Context context) {
        String path = event.getDataItem().getUri().getPath();
        DataMap dataMap = DataMapItem.fromDataItem(event.getDataItem()).getDataMap();
        switch (path) {
            case SynDataUtils.PARAMS:
                saveParams(dataMap, context);
                break;
            case SynDataUtils.DEVICE:
                saveDevice(dataMap, context);
                break;
        }

    }

    private void saveDevice(DataMap dataMap, Context context) {
        DeviceSettingsAdapter.save(dataMap, new StoragePreference(context));
        sendDataToWatchFace(SynDataUtils.ACTION_DEVICE_CHANGE, context);
    }

    private void saveParams(DataMap dataMap, Context context) {
        WatchParamAdapterImp watchParamAdapterImp = new WatchParamAdapterImp(context);
        watchParamAdapterImp.save(dataMap, new StoragePreference(context));
        sendDataToWatchFace(SynDataUtils.ACTION_PARAMS_CHANGE, context);
    }

    private void sendDataToWatchFace(String action, Context context) {
        Intent intent = new Intent(action);
        context.sendBroadcast(intent);
    }

}

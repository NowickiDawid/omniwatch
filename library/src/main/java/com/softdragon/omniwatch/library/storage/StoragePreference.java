package com.softdragon.omniwatch.library.storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dawid on 2016-02-18.
 */
public class StoragePreference {
    private final SharedPreferences mShared;

    public StoragePreference(Context context) {
        mShared = context.getSharedPreferences("storage", Context.MODE_PRIVATE);
    }

    public void putFloat(String key, float value) {
        mShared.edit().putFloat(key, value).apply();
    }

    public float getFloat(String key, float defualt) {
        return mShared.getFloat(key, defualt);
    }

    public void putBoolean(String key, boolean value) {
        mShared.edit().putBoolean(key, value).apply();
    }

    public boolean getBoolean(String key, boolean defualt) {
        return mShared.getBoolean(key, defualt);
    }

}

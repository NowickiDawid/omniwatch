package com.softdragon.omniwatch.library.sychronize;

/**
 * Created by Dawid on 2016-03-10.
 */
public interface SynInter {

    void onCreate();

    void onResume();

    void onPause();
}

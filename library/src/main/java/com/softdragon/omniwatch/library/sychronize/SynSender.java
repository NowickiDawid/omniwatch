package com.softdragon.omniwatch.library.sychronize;

import android.os.Bundle;

import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.Params;

/**
 * Created by Dawid on 2016-03-10.
 */
public interface SynSender extends SynInter {
    void sendParams(Params params);

    void sendDevice(DeviceSettings settings);
}


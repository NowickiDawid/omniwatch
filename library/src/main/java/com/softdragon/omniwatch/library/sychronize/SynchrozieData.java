package com.softdragon.omniwatch.library.sychronize;

/**
 * Created by Dawid on 2016-03-10.
 */
public abstract class SynchrozieData {
    SynSender mSynSender;
    SynReceiver mSynReceiver;

    public SynchrozieData() {
    }

    public void setSynSender(SynSender synSender) {
        mSynSender = synSender;
    }

    public void setSynReceiver(SynReceiver synReceiver) {
        mSynReceiver = synReceiver;
    }

    public void onCreate() {
        mSynReceiver.onCreate();
        mSynSender.onCreate();
    }

    public void onResume() {
        mSynReceiver.onResume();
        mSynSender.onResume();
    }


    public void onPause() {
        mSynReceiver.onPause();
        mSynSender.onPause();
    }


    public SynReceiver getSynReceiver() {
        return mSynReceiver;
    }

    public SynSender getSynSender() {
        return mSynSender;
    }

}

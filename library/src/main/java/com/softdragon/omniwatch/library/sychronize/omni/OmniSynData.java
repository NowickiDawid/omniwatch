package com.softdragon.omniwatch.library.sychronize.omni;

import android.content.Context;

import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.DeviceSettingsAdapter;
import com.softdragon.omniwatch.library.presenter.params.WatchParamAdapter;
import com.softdragon.omniwatch.library.storage.StoragePreference;
import com.softdragon.omniwatch.library.sychronize.SynchrozieData;
import com.softdragon.omniwatch.library.sychronize.wear.SynReceiverBluetooth;
import com.softdragon.omniwatch.library.sychronize.wear.SynSenderBluetooth;

/**
 * Created by Dawid on 2016-03-10.
 */
public class OmniSynData extends SynchrozieData {
    private final ReceiverListener mListener;
    private StoragePreference mStorage;
    private Params mParams = new Params();
    private DeviceSettings mDeviceSettings;
    private WatchParamAdapter mWatchParamAdapter;

    public OmniSynData(Context context, ReceiverListener listener) {
        setSynSender(new SynSenderBluetooth(context));
        setSynReceiver(new OmniSynReceiverBluetooth(context));
        mWatchParamAdapter = new WatchParamAdapterImp(context);
        mStorage = new StoragePreference(context);
        mParams = mWatchParamAdapter.load(mStorage);
        mDeviceSettings = DeviceSettingsAdapter.load(mStorage);
        mListener = listener;
    }

    public void setParams(Params params) {
        mParams = params;
        mWatchParamAdapter.save(mParams, mStorage);
        getSynSender().sendParams(params);
    }

    public void setDeviceSettings(DeviceSettings settings) {
        mDeviceSettings = settings;
        DeviceSettingsAdapter.save(mDeviceSettings, mStorage);
        getSynSender().sendDevice(settings);
    }

    public DeviceSettings getDeviceSettings() {
        return mDeviceSettings;
    }

    public Params getParams() {
        return mParams;
    }

    class OmniSynReceiverBluetooth extends SynReceiverBluetooth {

        public OmniSynReceiverBluetooth(Context context) {
            super(context);
        }

        @Override
        public void receiverParams(Params params) {
            mParams = params;
            mListener.receiverParams(params);
        }

        @Override
        public void receiverDeviceSettings(DeviceSettings device) {
            mDeviceSettings = device;
            mListener.receiverDeviceSettings(device);
        }
    }

    public interface ReceiverListener {

        void receiverParams(Params params);

        void receiverDeviceSettings(DeviceSettings device);
    }
}

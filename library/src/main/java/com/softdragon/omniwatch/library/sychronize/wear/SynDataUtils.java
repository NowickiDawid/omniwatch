package com.softdragon.omniwatch.library.sychronize.wear;

/**
 * Created by Dawid on 2016-03-12.
 */
public class SynDataUtils {
    public static final String ACTION_PARAMS_CHANGE = "ACTION_PARAMS_CHANGE";
    public static final String PARAMS = "/params";

    public static final String ACTION_DEVICE_CHANGE = "ACTION_DEVICE_CHANGE";
    public static final String DEVICE = "/device";
}

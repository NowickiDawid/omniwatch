package com.softdragon.omniwatch.library.sychronize.wear;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.DeviceSettingsAdapter;
import com.softdragon.omniwatch.library.presenter.params.WatchParamAdapter;
import com.softdragon.omniwatch.library.storage.StoragePreference;
import com.softdragon.omniwatch.library.sychronize.SynReceiver;

/**
 * Created by Dawid on 2016-03-10.
 */
public abstract class SynReceiverBluetooth implements SynReceiver {

    private final Context mContext;
    private final StoragePreference mStorage;
    private final WatchParamAdapter mWatchParamImp;

    public SynReceiverBluetooth(Context context) {
        mContext = context;
        mStorage = new StoragePreference(context);
        mWatchParamImp = new WatchParamAdapterImp(context);
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onResume() {
        IntentFilter filter = new IntentFilter(SynDataUtils.ACTION_PARAMS_CHANGE);
        mContext.registerReceiver(mDataReceiver, filter);
        IntentFilter filterDevice = new IntentFilter(SynDataUtils.ACTION_DEVICE_CHANGE);
        mContext.registerReceiver(mDeviceReceiver, filterDevice);

    }

    @Override
    public void onPause() {
        mContext.unregisterReceiver(mDataReceiver);
        mContext.unregisterReceiver(mDeviceReceiver);
    }

    final BroadcastReceiver mDataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            receiverParams(mWatchParamImp.load(mStorage));
        }
    };
    final BroadcastReceiver mDeviceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            receiverDeviceSettings(DeviceSettingsAdapter.load(mStorage));
        }
    };


}

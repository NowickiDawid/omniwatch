package com.softdragon.omniwatch.library.sychronize.wear;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.Wearable;
import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.data.SendDataByDataLayerThread;
import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.DeviceSettingsAdapter;
import com.softdragon.omniwatch.library.sychronize.SynSender;

/**
 * Created by Dawid on 2016-03-10.
 */
public class SynSenderBluetooth implements SynSender, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "SynSenderBluetooth";
    private final WatchParamAdapterImp mWatchParamAdapter;
    private android.content.Context context;
    private GoogleApiClient mGoogleApiClient;

    public SynSenderBluetooth(Context context) {
        this.context = context;
        mWatchParamAdapter = new WatchParamAdapterImp(context);
    }

    @Override
    public void onCreate() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onResume() {
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        mGoogleApiClient.disconnect();
    }

    protected void sendDataToWatch(DataMap dataMap, String path) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
            new SendDataByDataLayerThread(path, dataMap, mGoogleApiClient).start();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        OmniLog.i(TAG, "onConnected");
    }

    @Override
    public void onConnectionSuspended(int i) {
        OmniLog.i(TAG, "onConnectionSuspended" + i);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        OmniLog.i(TAG, "onConnectionFailed" + connectionResult.toString());
    }

    @Override
    public void sendParams(Params params) {
        sendDataToWatch(mWatchParamAdapter.getData(params), SynDataUtils.PARAMS);
    }

    @Override
    public void sendDevice(DeviceSettings settings) {
        sendDataToWatch(DeviceSettingsAdapter.getData(settings), SynDataUtils.DEVICE);

    }
}

package com.softdragon.omniwatch.library.timemode;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dawid on 2016-03-31.
 */
public class SpeedTimeMode implements TimeModeService.TimeMode {
    private Calendar mCalendar;
    private long mTime;

    @Override
    public Calendar getCalendar() {
        return mCalendar;
    }

    @Override
    public long getAmbitionModeTimerRate() {
        return TimeUnit.SECONDS.toMillis(1);
    }

    @Override
    public long getStandardModeTimerRate() {
        return TimeUnit.MILLISECONDS.toMillis(20);
    }

    @Override
    public void createCalendar() {
        mCalendar = Calendar.getInstance();
        mTime = System.currentTimeMillis();
    }

    @Override
    public void setTimeZone(TimeZone aDefault) {
        mCalendar.setTimeZone(aDefault);
    }

    @Override
    public void refreshTime() {
        mCalendar.setTimeInMillis(System.currentTimeMillis() + ((System.currentTimeMillis() - mTime) * 16));
    }
}

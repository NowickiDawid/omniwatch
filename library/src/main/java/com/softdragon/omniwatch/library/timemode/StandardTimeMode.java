package com.softdragon.omniwatch.library.timemode;

import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dawid on 2016-03-31.
 */
public class StandardTimeMode implements TimeModeService.TimeMode {
    private Calendar mCalendar;

    @Override
    public Calendar getCalendar() {
        return mCalendar;
    }

    @Override
    public long getAmbitionModeTimerRate() {
        return TimeUnit.MINUTES.toMillis(1);
    }

    @Override
    public long getStandardModeTimerRate() {
        return TimeUnit.SECONDS.toMillis(1);
    }

    @Override
    public void createCalendar() {
        mCalendar = Calendar.getInstance();
    }

    @Override
    public void setTimeZone(TimeZone aDefault) {
        mCalendar.setTimeZone(aDefault);
    }

    @Override
    public void refreshTime() {
        mCalendar.setTimeInMillis(System.currentTimeMillis());
    }
}

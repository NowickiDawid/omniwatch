package com.softdragon.omniwatch.library.timemode;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by Dawid on 2016-03-31.
 */
public class TimeModeService {

    public TimeMode createTimeMode() {
        return new StandardTimeMode();
    }

    public interface TimeMode {
        Calendar getCalendar();

        long getAmbitionModeTimerRate();

        long getStandardModeTimerRate();

        void createCalendar();

        void setTimeZone(TimeZone aDefault);

        void refreshTime();
    }
}

package com.softdragon.omniwatch.library.utils;

/**
 * Created by Dawid on 2016-03-31.
 */
public class TextUtils {
    public static String getCapFirst(String name) {
        return Character.toUpperCase(name.charAt(0)) + name.substring(1);

    }
    public static String getCaptionText(String name) {
        return name.toUpperCase();

    }
}

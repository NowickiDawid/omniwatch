package com.softdragon.omniwatch.library.utils.arcs;

/**
 * Created by Dawid on 2016-04-18.
 */
public interface DegreeDescription {
    int getMaxDegree();

    int getMinDegree();

    int getMaxValue();

    int getMinValue();
}

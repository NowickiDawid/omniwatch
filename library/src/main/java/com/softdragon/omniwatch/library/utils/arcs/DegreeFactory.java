package com.softdragon.omniwatch.library.utils.arcs;

import android.util.Log;

import java.util.HashMap;

/**
 * Created by Dawid on 2016-04-18.
 */
public class DegreeFactory {
    private static final String TAG = "DegreeFactory";
    HashMap<String, float[]> mKnownDegree = new HashMap<>();

    public float[] createDegree(DegreeDescription degreeDescription) {
        String key = createKey(degreeDescription);
        float[] degrees = mKnownDegree.get(key);
        if (degrees != null) {
            Log.d(TAG, "Get " + key);
            return degrees;
        }
        Log.d(TAG, "Crete " + key);
        degrees = generateDegrees(degreeDescription);
        mKnownDegree.put(key, degrees);
        return degrees;
    }

    private String createKey(DegreeDescription degreeDescription) {
        return degreeDescription.getMaxDegree() + "-" + degreeDescription.getMinDegree() + " " + degreeDescription.getMaxValue() + "-" + degreeDescription.getMinValue();
    }

    private float[] generateDegrees(DegreeDescription degreeDescription) {
        float degrees[] = new float[degreeDescription.getMaxValue() - degreeDescription.getMinValue()];
        int degree = (degreeDescription.getMaxDegree() - degreeDescription.getMinDegree()) / degrees.length;
        for (int i = 0; i < degrees.length; i++)
            degrees[i] = degreeDescription.getMinDegree() + degree * i;
        return degrees;

    }
}

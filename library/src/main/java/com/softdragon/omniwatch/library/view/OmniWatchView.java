package com.softdragon.omniwatch.library.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;


import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.WatchPresenter;
import com.softdragon.omniwatch.library.utils.arcs.DegreeDescription;
import com.softdragon.omniwatch.library.utils.arcs.DegreeFactory;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.RotatingBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;
import com.softdragon.omniwatch.library.view.element.utils.TimeCounter;
import com.softdragon.omniwatch.library.viewmodel.OmniWatchViewModel;

import java.util.Calendar;

/**
 * Created by Dawid on 2016-02-06.
 */
public class OmniWatchView {
    private static final String TAG = "OmniWatchView";
    private final WatchPresenter mWatchPresenter;
    private BackgroundGraphicElement mBackground;
    private GraphicElement mNormalModeGraphicElements[];
    private LifeCycleListeners mLifeCycleListeners[];
    private OmniWatchViewModel mViewModel;
    private OnWatchListener mWatch;
    private GraphicElement mAmbitionModeGraphicElements[];
    private GraphicElement[] mDraw = new GraphicElement[]{};
    private TimeCounter mTimeCounter = new TimeCounter("onDraw");
    private DegreeFactory mDegreeFactory = new DegreeFactory();
    private boolean mIsNeedRedraw;
    private int mLastWidth = -1;
    private int mLastHeigh = -1;
    private OnDrawListener mOnDrawListener = new NoneOnDraw();
    private boolean m12Hour;

    public OmniWatchView(OnWatchListener watch, OmniWatchViewModel model) {
        mWatch = watch;
        mViewModel = model;
        mViewModel.setView(this);
        mWatchPresenter = new WatchPresenter(this);
    }

    private void createPaint() {
        mWatchPresenter.createPaint();
        refreshParams();
    }

    public Paint createStandardPaint() {
        return new Paint(Paint.ANTI_ALIAS_FLAG | Paint.DITHER_FLAG | Paint.FILTER_BITMAP_FLAG);
    }

    private void refreshParams() {
        mWatchPresenter.setParameters(mViewModel.getParams());
    }

    private void createGraphicContent() {
        if (mViewModel.isScreenRound()) {
            mBackground = mWatchPresenter.createRoundBackground();
            mLifeCycleListeners = mWatchPresenter.createLiveCycleListeners();
            mNormalModeGraphicElements = mWatchPresenter.createRoundNormalMode();
            mAmbitionModeGraphicElements = mWatchPresenter.createRoundAmbitionMode();
        } else {
            mBackground = mWatchPresenter.createSquareBackground();
            mLifeCycleListeners = mWatchPresenter.createLiveCycleListeners();
            mNormalModeGraphicElements = mWatchPresenter.createSquareNormalMode();
            mAmbitionModeGraphicElements = mWatchPresenter.createSquareAmbitionMode();
        }

    }

    public void changeTimeZone() {
        mViewModel.changeTimeZone();
    }

    public void onCreate() {
        long time = System.currentTimeMillis();
        mViewModel.onCreate();
        createPaint();
        createGraphicContent();
        for (GraphicElement element : mNormalModeGraphicElements)
            element.setGlobalView(this);
        for (GraphicElement element : mAmbitionModeGraphicElements)
            element.setGlobalView(this);
        onCreateDiffrentMods();
        for (LifeCycleListeners element : mLifeCycleListeners)
            element.onCreate(getContext());
        setAmbientMode(mWatch.isInAmbientMode());
        Log.d(TAG, String.format("OnCreate %d", (System.currentTimeMillis() - time)));

    }

    public void onRecreate() {
        onDestroy();
        onCreate();
        if (mLastHeigh != -1 && mLastWidth != -1)
            onSurfaceChange(mLastWidth, mLastHeigh);
        mIsNeedRedraw = true;
    }

    private void onCreateDiffrentMods() {
        for (GraphicElement element : mNormalModeGraphicElements)
            element.onCreate();
        for (GraphicElement element : mAmbitionModeGraphicElements)
            element.onCreate();
    }

    public void onDestroy() {
        mViewModel.onDestroy();
        for (GraphicElement element : mNormalModeGraphicElements)
            element.onDestroy();
        for (GraphicElement element : mAmbitionModeGraphicElements)
            element.onDestroy();
        for (LifeCycleListeners element : mLifeCycleListeners)
            element.onDestroy(getContext());
        mDraw = new GraphicElement[0];
    }

    public void setLowBitAmbientMode(boolean isLow) {
        mViewModel.setLowBitAmbientMode(isLow);
    }

    public void setAmbientMode(boolean inAmbientMode) {
        if (mViewModel.isAmbientMode() != inAmbientMode || mDraw.length == 0) {
            mIsNeedRedraw = true;
            mViewModel.setAmbientMode(inAmbientMode);
            mDraw = inAmbientMode ? mAmbitionModeGraphicElements : mNormalModeGraphicElements;
            mWatch.invalidate();
            mTimeCounter.reset();
        }
    }

    public void onTouchScreen(int x, int y, long eventTime) {
        mViewModel.onTouchScreen(x, y, eventTime);
    }

    public void onTouchCancel(int x, int y, long eventTime) {
        mViewModel.onTouchCancel(x, y, eventTime);
    }

    public void onTap(int x, int y, long eventTime) {
        mViewModel.onTap(x, y, eventTime);
    }

    public void onDraw(Canvas canvas, Rect bounds) {
        mOnDrawListener.onDraw(canvas, bounds);
    }

    public void onTimeTick() {
        mWatch.invalidate();
    }

    public Calendar getCalendar() {
        return mViewModel.getCalendar();
    }

    public Resources getResources() {
        return mWatch.getContext().getResources();
    }

    public Context getContext() {
        return mWatch.getContext();
    }

    public void onSurfaceChange(int width, int height) {
        mLastWidth = width;
        mLastHeigh = height;
        PointF point = mBackground.getScala(width, height);
        scalDiffrentMods(point.x, point.y);
        mOnDrawListener = new NormalDraw();
    }

    private void scalDiffrentMods(float scalaX, float scalaY) {
        for (GraphicElement element : mNormalModeGraphicElements)
            element.onScaleChange(scalaX, scalaY);
        for (GraphicElement element : mAmbitionModeGraphicElements)
            element.onScaleChange(scalaX, scalaY);
    }

    public void invalidateAll() {
        mWatch.invalidate();
    }

    public boolean shouldTimerBeRunning() {
        return mWatch.shouldTimerBeRunning();
    }

    public void updateTimer() {
        mViewModel.updateTimer();
    }

    public Typeface getFont(String file) {
        return Typeface.createFromAsset(getContext().getAssets(), file);
    }

    public float getSizeFromResources(int resource) {
        return getContext().getResources().getDimensionPixelSize(resource);
    }

    public void setNeedRedraw(boolean needRedraw) {
        mIsNeedRedraw = needRedraw;
    }

    public void setParams(Params params) {
        mIsNeedRedraw = true;
        mViewModel.setParams(params);
        refreshParams();
        invalidateAll();
    }


    public boolean isNeedRedraw() {
        return mIsNeedRedraw;
    }

    public void setWatchScreenParams(boolean round, int bottomChin) {
        boolean isOldRound = mViewModel.isScreenRound();
        mViewModel.setIsWatchScreenParamsSetted(true);
        mViewModel.setIsScreenRound(round);
        mViewModel.setBottomChin(bottomChin);
        if (mBackground != null && isOldRound != mViewModel.isScreenRound())
            onRecreate();
    }

    public boolean isWatchScreenParamsSetted() {
        return mViewModel.isWatchScreenParamsSetted();
    }

    public boolean isScreenRound() {
        return mViewModel.isScreenRound();
    }

    public int getBottomChin() {
        return mViewModel.getBottomChin();
    }

    public float[] getDegree(DegreeDescription description) {
        return mDegreeFactory.createDegree(description);
    }

    public float[] getDegrees(DegreeDescription description) {
        return mDegreeFactory.createDegree(description);
    }

    public void set12Hour(boolean is12Hour) {
        this.m12Hour = is12Hour;
    }

    public boolean is12Hour() {
        return m12Hour;
    }

    public interface OnWatchListener {

        Context getContext();

        void invalidate();

        boolean shouldTimerBeRunning();

        boolean isInAmbientMode();
    }

    class NormalDraw implements OnDrawListener {

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            // Draw the background.
            mTimeCounter.start();
            mViewModel.onDraw();
            for (GraphicElement element : mDraw) {
                element.onDraw(canvas, bounds);
                OmniLog.i(TAG, String.format("--onDraw(%s)", element.toString()));
            }
            mTimeCounter.end();
            mIsNeedRedraw = false;
        }
    }

    class NoneOnDraw implements OnDrawListener {

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {

        }
    }

    interface OnDrawListener {
        void onDraw(Canvas canvas, Rect bounds);
    }

}

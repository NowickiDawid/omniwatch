package com.softdragon.omniwatch.library.view.element;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-02-07.
 */
public  class BackgroundGraphicElement extends GraphicElement {

    private final Paint mPaint;
    public BitmapPart mBitmapPart;

    public BackgroundGraphicElement(BackgroundGraphicDescription description) {
        mBitmapPart = description.getmBitmapPart();
        mPaint = description.createPaint();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBitmapPart.onCreate(mGlobalView.getContext());
        mBitmapPart.scalBitmap();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBitmapPart.onDestroy();
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        canvas.drawBitmap(mBitmapPart.getScaledBitmap(0), 0, 0, mPaint);
    }

    @Override
    public void onScaleChange(float scalaX, float scalaY) {
        super.onScaleChange(scalaX, scalaY);
        mBitmapPart.onScaleChange(scalaX, scalaY);
    }

    public PointF getScala(int width, int height) {
        return mBitmapPart.getScale(width, height);
    }

    public interface BackgroundGraphicDescription {
        BitmapPart getmBitmapPart();

        Paint createPaint();
    }
}

package com.softdragon.omniwatch.library.view.element;

import android.graphics.Paint;

import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.text.description.BitmapGraphicElementDescription;
import com.softdragon.omniwatch.library.view.element.text.description.LocationGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-03-24.
 */
public abstract class BitmapGraphicElement<T extends BitmapGraphicElementDescription> extends LocatedGraphicElement<T> {
    protected BitmapPart mBitmapPart;
    protected Paint mPaint;

    public BitmapGraphicElement(T description) {
        super(description);
        mBitmapPart = description.createBitmapPart();
        mPaint = description.createPaint();
    }

    @Override
    public void onCreate() {
        mBitmapPart.onCreate(mGlobalView.getContext());
        mBitmapPart.scalBitmap();
    }

    @Override
    public void setGlobalView(OmniWatchView globalView) {
        super.setGlobalView(globalView);
        if (mPaint == null) {
            mPaint = mGlobalView.createStandardPaint();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBitmapPart.onDestroy();
    }

    @Override
    public void onScaleChange(float scalaX, float scalaY) {
        super.onScaleChange(scalaX, scalaY);
        mBitmapPart.onScaleChange(scalaX, scalaY);
    }

}

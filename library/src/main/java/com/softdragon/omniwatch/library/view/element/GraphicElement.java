package com.softdragon.omniwatch.library.view.element;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.OmniWatchView;


/**
 * Created by Dawid on 2016-02-06.
 */
public abstract class GraphicElement {
    protected OmniWatchView mGlobalView;

    public void onCreate() {
    }

    public abstract void onDraw(Canvas canvas, Rect bounds);

    public void setGlobalView(OmniWatchView globalView) {
        mGlobalView = globalView;
    }


    public void onScaleChange(float scalaX, float scalaY) {

    }

    public void onDestroy() {

    }
}

package com.softdragon.omniwatch.library.view.element;

import android.graphics.Canvas;

import com.softdragon.omniwatch.library.view.element.text.description.LocationGraphicDescription;

/**
 * Created by Dawid on 2016-02-07.
 */
public abstract class LocatedGraphicElement<T extends LocationGraphicDescription> extends GraphicElement {
    private T mDescription;

    public LocatedGraphicElement(T description) {
        mDescription = description;
    }

    public T getDescription() {
        return mDescription;
    }

    protected float getY(Canvas canvas, float height) {
        return canvas.getHeight() * getDescription().getPercentY() - height;
    }

    protected float getX(Canvas canvas, float width) {
        return canvas.getWidth() * getDescription().getPercentX() - width;
    }
}

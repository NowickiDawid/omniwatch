package com.softdragon.omniwatch.library.view.element;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.element.text.description.BitmapGraphicElementDescription;
import com.softdragon.omniwatch.library.view.element.text.description.LocationGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-02-07.
 */
public abstract class MultiBitmapMultiStateGraphicElement extends BitmapGraphicElement<MultiBitmapMultiStateGraphicElement.MultiBitmapMultiStateGraphicsDescription> {
    private Bitmap[] mBitmaps;
    private int mLastValue;

    public MultiBitmapMultiStateGraphicElement(MultiBitmapMultiStateGraphicsDescription description) {
        super(description);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        int value = getDescription().getValue();
        if (value == mLastValue && mBitmaps != null) {
            onDrawBitmap(canvas, mBitmaps);
            return;
        }
        mLastValue = value;
        mBitmaps = new Bitmap[getDescription().getSize()];
        String valueString = String.valueOf(value);
        int sizeDiff = getDescription().getSize() - valueString.length();
        if (sizeDiff < 0)
            sizeDiff = 0;
        int maxSize = valueString.length() < mBitmaps.length ? valueString.length() : mBitmaps.length;
        for (int i = 0; i < sizeDiff; i++) {
            mBitmaps[i] = mBitmapPart.getScaledBitmap(0);
        }
        for (int i = 0; i < maxSize; i++) {
            mBitmaps[i + sizeDiff] = mBitmapPart.getScaledBitmap(valueString.charAt(i) - '0');
        }

        onDrawBitmap(canvas, mBitmaps);
    }
    protected abstract void onDrawBitmap(Canvas canvas, Bitmap[] bitmaps);


    public interface MultiBitmapMultiStateGraphicsDescription extends BitmapGraphicElementDescription {
        int getValue();

        int getSize();
    }
}

package com.softdragon.omniwatch.library.view.element;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.element.text.description.BitmapGraphicElementDescription;
import com.softdragon.omniwatch.library.view.element.text.description.LocationGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-02-07.
 */
public  class OneBitmapMultiStateGraphicElement extends BitmapGraphicElement<OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription> {
    private static final String TAG = "OneBitmapMultiStateGraphicElement" ;
    private int mLastValue;

    public OneBitmapMultiStateGraphicElement(OneBitmapMultiStateGraphicsDescription description) {
        super(description);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        int value = getDescription().getValue();
        if (value == mLastValue ) {
            onDrawBitmap(canvas);
            return;
        }
        mLastValue = value;
        if (mLastValue >= mBitmapPart.size())
            mLastValue = 0;
        onDrawBitmap(canvas);
    }

    private void onDrawBitmap(Canvas canvas) {
        if(mLastValue==-1)
            return;
        Bitmap bitmap = mBitmapPart.getScaledBitmap(mLastValue);
        float x = getX(canvas, bitmap.getWidth() / 2);
        float y = getY(canvas, bitmap.getHeight() / 2);
        canvas.drawBitmap(bitmap, x, y, mPaint);
    }

    public interface OneBitmapMultiStateGraphicsDescription extends BitmapGraphicElementDescription {
        int getValue();
    }
}

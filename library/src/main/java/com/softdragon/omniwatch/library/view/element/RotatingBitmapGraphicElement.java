package com.softdragon.omniwatch.library.view.element;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.utils.arcs.DegreeDescription;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.text.description.BitmapGraphicElementDescription;

/**
 * Created by Dawid on 2016-02-06.
 */
public class RotatingBitmapGraphicElement extends BitmapGraphicElement<RotatingBitmapGraphicElement.RotatingBitmapGraphicDescription> {
    private static final String TAG = "GraphicBackgroundGraphicElement";
    private final int mMinValue;
    private final float mRotationX;
    private final float mRotationY;
    private float[] mDegrees;

    public RotatingBitmapGraphicElement(RotatingBitmapGraphicDescription description) {
        super(description);
        mMinValue = getDescription().getMinValue();
        mRotationX = getDescription().getRotationX();
        mRotationY = getDescription().getRotationY();
    }

    @Override
    public void setGlobalView(OmniWatchView globalView) {
        super.setGlobalView(globalView);
        mDegrees = globalView.getDegrees(getDescription());
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        Bitmap bitmap = mBitmapPart.getScaledBitmap(0);
        float x = getX(canvas, bitmap.getWidth() / 2);
        float y = getY(canvas, bitmap.getHeight());
        int value = getDescription().getValue();
        canvas.save();
        canvas.rotate(getDegree(value), canvas.getWidth() * mRotationX, canvas.getHeight() * mRotationY);
        canvas.drawBitmap(bitmap, x, y, mPaint);
        canvas.restore();

    }


    private float getDegree(int value) {
        return mDegrees[value - mMinValue];
    }


    public interface RotatingBitmapGraphicDescription extends DegreeDescription, BitmapGraphicElementDescription {

        int getValue();

        float getRotationY();

        float getRotationX();

    }
}

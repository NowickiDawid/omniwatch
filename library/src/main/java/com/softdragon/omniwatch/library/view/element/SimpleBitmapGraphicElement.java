package com.softdragon.omniwatch.library.view.element;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.element.text.description.BitmapGraphicElementDescription;
import com.softdragon.omniwatch.library.view.element.text.description.LocationGraphicDescription;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-02-06.
 */
public class SimpleBitmapGraphicElement extends BitmapGraphicElement<SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription> {
    private static final String TAG = "GraphicBackgroundGraphicElement";
    public SimpleBitmapGraphicElement(SimpleBitmapGraphicDescription description) {
        super(description);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        Bitmap bitmap = mBitmapPart.getScaledBitmap(0);
        float x = getX(canvas, bitmap.getWidth() / 2);
        float y = getY(canvas, bitmap.getHeight() / 2);
        canvas.drawBitmap(bitmap, x, y, mPaint);
    }

    public interface SimpleBitmapGraphicDescription extends BitmapGraphicElementDescription {

    }
}

package com.softdragon.omniwatch.library.view.element.background;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

import com.softdragon.omniwatch.library.view.element.GraphicElement;


/**
 * Created by Dawid on 2016-02-06.
 */
public class SimpleRoundeBackgroundGraphicElement extends GraphicElement {

    private final Paint mPaint = new Paint();

    public SimpleRoundeBackgroundGraphicElement(int normalColor) {
        mPaint.setColor(normalColor);
        mPaint.setAntiAlias(true);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        canvas.drawOval(new RectF(0, 0, canvas.getWidth(), canvas.getHeight()), mPaint);
    }
}

package com.softdragon.omniwatch.library.view.element.background;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.element.GraphicElement;


/**
 * Created by Dawid on 2016-02-06.
 */
public class SimpleSquareBackgroundGraphicElement extends GraphicElement {

    private final int mNormalColor;

    public SimpleSquareBackgroundGraphicElement(int normalColor) {
        mNormalColor = normalColor;
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        canvas.drawColor(mNormalColor);
    }
}

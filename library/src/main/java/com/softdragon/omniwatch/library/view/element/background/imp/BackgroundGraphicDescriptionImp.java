package com.softdragon.omniwatch.library.view.element.background.imp;

import android.graphics.Paint;

import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-06-20.
 */
public abstract class BackgroundGraphicDescriptionImp implements BackgroundGraphicElement.BackgroundGraphicDescription {
    @Override
    public Paint createPaint() {
        return null;
    }

}

package com.softdragon.omniwatch.library.view.element.battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.util.Log;

import com.softdragon.omniwatch.library.view.element.OneBitmapMultiStateGraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

/**
 * Created by Dawid on 2016-02-07.
 */
public abstract class BatteryLevelGraphicDescription implements OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription, LifeCycleListeners {
    private static final String TAG = "BatteryLevelGraphicDescription";
    private final int mMax;
    public int batteryValue = 0;
    public PowerConnectionReceiver mPowerConnectionReceiver = new PowerConnectionReceiver();

    @Override
    public int getValue() {
        return batteryValue;
    }

    @Override
    public void onCreate(Context context) {
        context.registerReceiver(mPowerConnectionReceiver, new IntentFilter((Intent.ACTION_BATTERY_CHANGED)));
    }

    @Override
    public void onDestroy(Context context) {
        context.unregisterReceiver(mPowerConnectionReceiver);

    }

    public BatteryLevelGraphicDescription(int max) {
        mMax=max;
    }

    public class PowerConnectionReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int scala = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 0);
            float batteryPct = level / (float) scala;
            batteryValue = Math.round(mMax * batteryPct);
            Log.d(TAG,String.format("Battery level %d , scale %d, batteryProc %f, batteryValue%d",level,scala,batteryPct,batteryValue));
        }
    }
}

package com.softdragon.omniwatch.library.view.element.clock;

import android.graphics.Paint;

import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.RotatingBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.group.description.GroupGraphicDescription;
import com.softdragon.omniwatch.library.view.element.text.description.LocationGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

import java.util.Calendar;

/**
 * Created by Dawid on 2016-03-26.
 */
public abstract class ClockBitmapGraphicDescription implements LocationGraphicDescription {

    public abstract Calendar getCalendar();

    public abstract float getPercentChinY();

    public abstract float getPercentSecondChinX();

    public abstract float getPercentSecondChinY();

    public abstract BitmapPart createHourBitmapPart();

    public abstract Paint getClockPaint();

    public abstract BitmapPart createMinuteBitmapPart();

    public abstract BitmapPart createSecondBitmapPart();

}

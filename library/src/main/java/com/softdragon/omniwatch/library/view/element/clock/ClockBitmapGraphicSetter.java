package com.softdragon.omniwatch.library.view.element.clock;

import android.graphics.Paint;

import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.RotatingBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Dawid on 2016-04-18.
 */
public class ClockBitmapGraphicSetter {
    public static GraphicElement createSecondGraphicElement(final ClockBitmapGraphicDescription description) {
        return new RotatingBitmapGraphicElement(new ClockViseRotatingBitmapGraphicDescription() {

            @Override
            public BitmapPart createBitmapPart() {
                return description.createSecondBitmapPart();
            }

            @Override
            public Paint createPaint() {
                return description.getClockPaint();
            }

            @Override
            protected int getField() {
                return Calendar.SECOND;
            }

            @Override
            public float getPercentX() {
                return description.getPercentSecondChinX();
            }

            @Override
            public float getPercentY() {
                return description.getPercentSecondChinY();
            }

            @Override
            public int getValue() {
                return description.getCalendar().get(getField());
            }

            @Override
            public float getRotationX() {
                return description.getPercentX();
            }

            @Override
            public float getRotationY() {
                return description.getPercentY();
            }
        });
    }

    public static GraphicElement creteMinuteGraphicElement(final ClockBitmapGraphicDescription description) {
        return new RotatingBitmapGraphicElement(new ClockViseRotatingBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return description.createMinuteBitmapPart();
            }

            @Override
            public Paint createPaint() {
                return description.getClockPaint();
            }

            @Override
            protected int getField() {
                return Calendar.MINUTE;
            }

            @Override
            public float getPercentX() {
                return description.getPercentX();
            }

            @Override
            public float getPercentY() {
                return description.getPercentChinY();
            }

            @Override
            public int getValue() {
                return description.getCalendar().get(getField());
            }

            @Override
            public float getRotationX() {
                return description.getPercentX();
            }

            @Override
            public float getRotationY() {
                return description.getPercentY();
            }
        });
    }

    public static GraphicElement createHourGraphicElement(final ClockBitmapGraphicDescription description) {
        return new RotatingBitmapGraphicElement(new RondRotatingBitmapGraphicDescription() {
            @Override
            public int getMaxValue() {
                return 60;
            }

            @Override
            public int getMinValue() {
                return 0;
            }

            @Override
            public int getValue() {
                return description.getCalendar().get(Calendar.HOUR) * 5 + (description.getCalendar().get(Calendar.MINUTE) / 12);
            }

            @Override
            public BitmapPart createBitmapPart() {
                return description.createHourBitmapPart();
            }

            @Override
            public Paint createPaint() {
                return description.getClockPaint();
            }


            @Override
            public float getPercentX() {
                return description.getPercentX();
            }

            @Override
            public float getPercentY() {
                return description.getPercentChinY();
            }

            @Override
            public float getRotationX() {
                return description.getPercentX();
            }


            @Override
            public float getRotationY() {
                return description.getPercentY();
            }
        });
    }
    private void addMinuteAndHours(ClockBitmapGraphicDescription clockColorBitmapGraphicDescription, ClockBitmapGraphicDescription clockBitmapGraphicDescription, List<GraphicElement> list) {
        list.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockColorBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockColorBitmapGraphicDescription));
    }
    static abstract class ClockViseRotatingBitmapGraphicDescription extends RondRotatingBitmapGraphicDescription {

        @Override
        public int getMaxValue() {
            return Calendar.getInstance().getMaximum(getField()) + 1;
        }

        @Override
        public int getMinValue() {
            return Calendar.getInstance().getMinimum(getField());
        }

        abstract protected int getField();


    }

    static abstract class RondRotatingBitmapGraphicDescription implements RotatingBitmapGraphicElement.RotatingBitmapGraphicDescription {

        @Override
        public int getMaxDegree() {
            return 360;
        }

        @Override
        public int getMinDegree() {
            return 0;
        }
    }

}

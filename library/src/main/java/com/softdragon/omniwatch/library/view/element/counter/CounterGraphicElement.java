package com.softdragon.omniwatch.library.view.element.counter;

import android.graphics.Bitmap;
import android.graphics.Canvas;

import com.softdragon.omniwatch.library.view.element.MultiBitmapMultiStateGraphicElement;

/**
 * Created by Dawid on 2016-02-07.
 */
public class CounterGraphicElement extends MultiBitmapMultiStateGraphicElement {
    public CounterGraphicElement(MultiBitmapMultiStateGraphicsDescription description) {
        super(description);
    }

    @Override
    protected void onDrawBitmap(Canvas canvas, Bitmap[] bitmaps) {
        int width = bitmaps[0].getWidth() * bitmaps.length;
        int height = bitmaps[0].getHeight();
        float positionX = getX(canvas, width / 2);
        float positionY = getY(canvas, height / 2);
        for (Bitmap bitmap : bitmaps) {
            canvas.drawBitmap(bitmap, positionX, positionY, null);
            positionX += bitmap.getWidth();
        }
    }

}

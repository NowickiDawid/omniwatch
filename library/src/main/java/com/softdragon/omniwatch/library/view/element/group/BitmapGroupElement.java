package com.softdragon.omniwatch.library.view.element.group;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.group.description.GroupGraphicDescription;

/**
 * Created by Dawid on 2016-03-20.
 */
public class BitmapGroupElement extends GroupGraphicElement<BitmapGroupElement.BitmapGroupDescription> {
    private static final String TAG = "BitmapGroupElement";
    private int mValue;
    private Canvas mCanvas;
    Bitmap mBitmap;

    public BitmapGroupElement(BitmapGroupDescription description) {
        super(description);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        createBitmapIfNeed(bounds);
        if (isNeedRefresh()) {
            mValue = getDescription().getValue();
            OmniLog.i(TAG, "onDrawReDraw");
            for (GraphicElement element : mGraphicElement) {
                long time = System.currentTimeMillis();
                element.onDraw(mCanvas, bounds);
                OmniLog.i(TAG, String.format("--onDraw(%s time %d)", element.toString(), System.currentTimeMillis() - time));
            }
        }
        canvas.drawBitmap(mBitmap, 0, 0, null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mBitmap != null) {
            mBitmap.recycle();
            mBitmap = null;
        }
    }

    protected boolean isNeedRefresh() {
        return getDescription().isNeedRedraw() || mValue != getDescription().getValue();
    }


    private void createBitmapIfNeed(Rect bounds) {
        if (mBitmap == null || bounds.height() != mBitmap.getHeight() || bounds.width() != mBitmap.getWidth()) {
            mBitmap = Bitmap.createBitmap(bounds.width(), bounds.height(), Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
        }
    }

    public interface BitmapGroupDescription extends GroupGraphicDescription {
        int getValue();

        boolean isNeedRedraw();
    }

}

package com.softdragon.omniwatch.library.view.element.group;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.group.description.GroupGraphicDescription;

/**
 * Created by Dawid on 2016-03-15.
 */
public abstract class GroupGraphicElement<T extends GroupGraphicDescription> extends GraphicElement {
    private static final String TAG = "GroupGraphicElement";
    private final T mDescription;
    protected GraphicElement[] mGraphicElement;

    public GroupGraphicElement(T description) {
        mDescription = description;
        mGraphicElement = description.getGraphicElement().toArray(new GraphicElement[1]);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        for (GraphicElement element : mGraphicElement) {
            element.onCreate();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for (GraphicElement element : mGraphicElement) {
            element.onDestroy();
        }
    }

    @Override
    public void onScaleChange(float scalaX, float scalaY) {
        super.onScaleChange(scalaX, scalaY);
        for (GraphicElement element : mGraphicElement) {
            element.onScaleChange(scalaX, scalaY);
        }
    }

    @Override
    public void setGlobalView(OmniWatchView globalView) {
        super.setGlobalView(globalView);
        for (GraphicElement element : mGraphicElement) {
            element.setGlobalView(globalView);
        }
    }

    public T getDescription() {
        return mDescription;
    }
}

package com.softdragon.omniwatch.library.view.element.group;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.group.description.GroupGraphicDescription;

/**
 * Created by Dawid on 2016-03-20.
 */
public abstract class GroupRedrawGroupElement extends GroupGraphicElement<GroupGraphicDescription> {
    private static final String TAG = "BitmapGroupElement";
    private int[] mValues;

    public GroupRedrawGroupElement(GroupGraphicDescription description) {
        super(description);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mValues=null;
    }

    @Override
    public void onScaleChange(float scalaX, float scalaY) {
        super.onScaleChange(scalaX, scalaY);
        mValues=null;
    }


    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        int[] values = getValues();
        if (values.length != mGraphicElement.length)
            throw new RuntimeException("Graphics and values need to have the same size");
        if (mValues == null)
            drawAll(canvas, bounds);
        else
            drawSome(canvas, bounds, values);
        mValues = values;
    }

    private void drawSome(Canvas canvas, Rect bounds, int[] values) {
        for (int i = 0; i < values.length; i++) {
            if (isNeedRefresh(values, i))
                mGraphicElement[i].onDraw(canvas, bounds);
        }
    }

    protected boolean isNeedRefresh(int[] values, int i) {
        return mValues[i] != values[i];
    }

    private void drawAll(Canvas canvas, Rect bounds) {
        for (GraphicElement element : mGraphicElement)
            element.onDraw(canvas, bounds);

    }

    protected abstract int[] getValues();


}

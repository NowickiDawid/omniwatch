package com.softdragon.omniwatch.library.view.element.group.description;

import com.softdragon.omniwatch.library.view.element.GraphicElement;

import java.util.List;

/**
 * Created by Dawid on 2016-03-20.
 */
public interface GroupGraphicDescription {
    List<GraphicElement> getGraphicElement();
}

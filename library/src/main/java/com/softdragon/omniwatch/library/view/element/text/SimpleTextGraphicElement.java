package com.softdragon.omniwatch.library.view.element.text;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;

import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;

/**
 * Created by Dawid on 2016-02-07.
 */
public class SimpleTextGraphicElement extends TextGraphicElement {
    public SimpleTextGraphicElement(TextGraphicDescription description) {
        super(description);
    }

    @Override
    protected void onDraw(Canvas canvas, RectF textRect, String value) {
        canvas.drawText(value, textRect.left, textRect.top, mPaint);
    }

}

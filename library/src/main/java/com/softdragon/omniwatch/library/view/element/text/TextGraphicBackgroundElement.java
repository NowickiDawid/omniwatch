package com.softdragon.omniwatch.library.view.element.text;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.view.element.text.description.BackgroundTextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;

/**
 * Created by Dawid on 2016-03-20.
 */
public class TextGraphicBackgroundElement extends TextGraphicElement {

    private Paint mPaintBackground;

    public TextGraphicBackgroundElement(BackgroundTextGraphicDescription description) {
        super(description);
        mPaintBackground = description.getBackgroundPaint();

    }


    @Override
    protected void onDraw(Canvas canvas, RectF textRect, String value) {
        RectF backGroundRectF = createBackgroundRect(textRect);
        canvas.drawRect(backGroundRectF, mPaintBackground);
        canvas.drawText(value, textRect.left, textRect.top, mPaint);
    }

    private RectF createBackgroundRect(RectF textRect) {

        return new RectF(textRect.left, textRect.top - textRect.height(), textRect.left + textRect.width() * 1.10f, textRect.top + textRect.height() * 0.1f);
    }
}

package com.softdragon.omniwatch.library.view.element.text;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.view.element.LocatedGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;

/**
 * Created by Dawid on 2016-03-20.
 */
public abstract class TextGraphicElement extends LocatedGraphicElement<TextGraphicDescription> {
    protected Paint mPaint;
    private float mHeight;

    public TextGraphicElement(TextGraphicDescription description) {
        super(description);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPaint = getDescription().getPaint();
        mPaint.setTextSize(getTextSize(1));
        mHeight = getTextHeight();
    }

    private float getTextSize(float scale) {
        return getDescription().getTextSize() * scale;
    }

    @Override
    public void onScaleChange(float scalaX, float scalaY) {
        super.onScaleChange(scalaX, scalaY);
        mPaint.setTextSize(getTextSize(scalaX));
        mHeight = getTextHeight();
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        String value = getDescription().getText();
        float width = getTextWidth(value);
        float x = getTextX(canvas, width);
        float y = getTextY(canvas, mHeight);
        onDraw(canvas, new RectF(x, y, x + width, y + mHeight), value);
    }

    abstract protected void onDraw(Canvas canvas, RectF textRect, String value);

    protected float getTextWidth(String value) {
        return mPaint.measureText(value);
    }

    protected float getTextY(Canvas canvas, float height) {
        return getY(canvas, -height / 2);
    }

    protected float getTextX(Canvas canvas, float width) {
        return getX(canvas, width / 2);
    }

    protected float getTextHeight() {
        String word="123456789";
        Rect textBound=new Rect();
        mPaint.getTextBounds(word, 0, word.length(), textBound);
        return textBound.height();
    }
}

package com.softdragon.omniwatch.library.view.element.text.description;

import android.graphics.Paint;

/**
 * Created by Dawid on 2016-03-20.
 */
public interface BackgroundTextGraphicDescription extends TextGraphicDescription {
    Paint getBackgroundPaint();

}
package com.softdragon.omniwatch.library.view.element.text.description;

import android.graphics.Paint;

import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

/**
 * Created by Dawid on 2016-03-24.
 */
public interface BitmapGraphicElementDescription extends LocationGraphicDescription {
    BitmapPart createBitmapPart();
    Paint createPaint();
}

package com.softdragon.omniwatch.library.view.element.text.description;

import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Dawid on 2016-02-07.
 */
public interface LocationGraphicDescription {
    float getPercentX();

    float getPercentY();

}

package com.softdragon.omniwatch.library.view.element.text.description;

import android.graphics.Paint;

/**
 * Created by Dawid on 2016-02-07.
 */
public interface TextGraphicDescription extends LocationGraphicDescription {
    Paint getPaint();

    String getText();

    float getTextSize();
}

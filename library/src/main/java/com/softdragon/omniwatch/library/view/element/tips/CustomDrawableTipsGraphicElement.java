package com.softdragon.omniwatch.library.view.element.tips;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;

import java.util.Calendar;

/**
 * Created by Dawid on 2016-02-07.
 */
public class CustomDrawableTipsGraphicElement extends GraphicElement {
    private final CustomDrawerTipsDescription mDescription;
    BitmapPart mBitmapPart;
    private int mValue;
    private Bitmap mRottateBitmap;
    private float mX;
    private float mY;
    private Paint mPaint;

    public CustomDrawableTipsGraphicElement(CustomDrawerTipsDescription description) {
        mDescription = description;
        mBitmapPart = description.createBitmapPart();
        mPaint = description.createPaint();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mBitmapPart.onCreate(mGlobalView.getContext());
        mBitmapPart.scalBitmap();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mBitmapPart.onDestroy();
    }

    @Override
    public void onScaleChange(float scalaX, float scalaY) {
        super.onScaleChange(scalaX, scalaY);
        mBitmapPart.onScaleChange(scalaX, scalaY);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {
        Calendar calendar = mGlobalView.getCalendar();
        int value = calendar.get(Calendar.HOUR);
        if (mValue == value && mRottateBitmap != null) {
            drawTip(canvas);
            return;
        }
        countRotation(canvas, bounds, value);
        drawTip(canvas);
    }

    private void countRotation(Canvas canvas, Rect bounds, int value) {
        mValue = value;
        float centerX = bounds.width() / 2f;
        float centerY = bounds.height() / 2f;
        float hrRot = value / 6f * (float) Math.PI;
        float hrLength = canvas.getHeight() / 2 * mDescription.getLocation();
        Bitmap bitmap = mBitmapPart.getScaledBitmap(value == 0 || value == 3 || value == 6 || value == 9 ? 0 : 1);
        int degree = (int) Math.toDegrees(hrRot) - 90;
        mRottateBitmap = createRotateBitmap(bitmap, degree);
        float hrX = (float) Math.sin(hrRot) * hrLength;
        float hrY = (float) -Math.cos(hrRot) * hrLength;
        mX = centerX + hrX - mRottateBitmap.getWidth() / 2;
        mY = centerY + hrY - mRottateBitmap.getHeight() / 2;
    }

    private void drawTip(Canvas canvas) {
        canvas.drawBitmap(mRottateBitmap, mX, mY, mPaint);
    }

    public static Bitmap createRotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public interface CustomDrawerTipsDescription {
        BitmapPart createBitmapPart();

        float getLocation();

        Paint createPaint();
    }
}

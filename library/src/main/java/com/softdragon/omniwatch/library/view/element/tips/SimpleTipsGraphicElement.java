package com.softdragon.omniwatch.library.view.element.tips;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;


import com.softdragon.omniwatch.library.R;
import com.softdragon.omniwatch.library.view.element.GraphicElement;

import java.util.Calendar;

/**
 * Created by Dawid on 2016-02-06.
 */
public class SimpleTipsGraphicElement extends GraphicElement {
    private final int mColor;
    private Paint mHandPaint;

    public SimpleTipsGraphicElement(int color) {
        mColor = color;
    }

    @Override
    public void onCreate() {
        mHandPaint = new Paint();
        mHandPaint.setColor(mColor);
        mHandPaint.setStrokeWidth(10);
        mHandPaint.setAntiAlias(true);
        mHandPaint.setStrokeCap(Paint.Cap.ROUND);
    }

    @Override
    public void onDraw(Canvas canvas, Rect bounds) {

        // Find the center. Ignore the window insets so that, on round watches with a
        // "chin", the watch face is centered on the entire screen, not just the usable
        // portion.
        float centerX = bounds.width() / 2f;
        float centerY = bounds.height() / 2f;
        Calendar calendar = mGlobalView.getCalendar();
        float secRot = calendar.get(Calendar.SECOND) / 30f * (float) Math.PI;
        int minutes = calendar.get(Calendar.MINUTE);
        float minRot = minutes / 30f * (float) Math.PI;
        float hrRot = ((calendar.get(Calendar.HOUR) + (minutes / 60f)) / 6f) * (float) Math.PI;

        float secLength = centerX - 20;
        float minLength = centerX - 40;
        float hrLength = centerX - 80;
/*
        if (!mGlobalView.isInAmbientMode()) {
            float secX = (float) Math.sin(secRot) * secLength;
            float secY = (float) -Math.cos(secRot) * secLength;
            canvas.drawLine(centerX, centerY, centerX + secX, centerY + secY, mHandPaint);
        }*/

        float minX = (float) Math.sin(minRot) * minLength;
        float minY = (float) -Math.cos(minRot) * minLength;
        canvas.drawLine(centerX, centerY, centerX + minX, centerY + minY, mHandPaint);

        float hrX = (float) Math.sin(hrRot) * hrLength;
        float hrY = (float) -Math.cos(hrRot) * hrLength;
        canvas.drawLine(centerX, centerY, centerX + hrX, centerY + hrY, mHandPaint);
    }
}

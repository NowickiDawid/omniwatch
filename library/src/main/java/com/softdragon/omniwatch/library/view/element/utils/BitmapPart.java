package com.softdragon.omniwatch.library.view.element.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;

/**
 * Created by Dawid on 2016-02-07.
 */
public abstract class BitmapPart<T>{
        private static final float MIN_DIF = 0.001f;
        private float mScalaX = 1;
        private float mScalaY = 1;
        protected Context mContext;
        private Bitmap mScaledBitmap[];
        private T mBitmapRes[];

        public BitmapPart(T[] bitmapRes) {
            mBitmapRes = bitmapRes;
        }

        public void onCreate(Context context) {
            mContext = context;
        }

        public void onDestroy() {
            for (int i = 0; i < mScaledBitmap.length; i++) {
                mScaledBitmap[i] = null;
            }
        }

        public Bitmap getScaledBitmap(int i) {
            return mScaledBitmap[i];
        }

        public void scalBitmap() {
            mScaledBitmap = new Bitmap[mBitmapRes.length];
            for (int i = 0; i < mBitmapRes.length; i++) {
                Bitmap bitmap = loadBitmap(mBitmapRes[i]);
                mScaledBitmap[i] = Bitmap.createScaledBitmap(bitmap,
                        (int) (bitmap.getWidth() * mScalaX), (int) (bitmap.getHeight() * mScalaY), true /* filter */);
            }
        }


        public int size() {
            return mBitmapRes.length;
        }

        protected abstract Bitmap loadBitmap(T res);

        public void onScaleChange(float scalaX, float scalaY) {
            if (mScaledBitmap == null
                    || Math.abs(mScalaY - scalaY) > MIN_DIF
                    || Math.abs(mScalaX - scalaX) > MIN_DIF) {
                mScalaX = scalaX;
                mScalaY = scalaY;
                scalBitmap();
            }
        }

        public PointF getScale(int width, int height) {
            Bitmap bitmap = loadBitmap(mBitmapRes[0]);
            return new PointF(width / (float) bitmap.getWidth(), height / (float) bitmap.getHeight());

        }

}


package com.softdragon.omniwatch.library.view.element.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.support.v4.content.ContextCompat;

/**
 * Created by Dawid on 2016-02-07.
 */
public class BitmapPartResource extends BitmapPart<Integer> {

    public BitmapPartResource(Integer... bitmapRes) {
        super(bitmapRes);
    }

    @Override
    protected Bitmap loadBitmap(Integer res) {
       return ((BitmapDrawable) ContextCompat.getDrawable(mContext,res)).getBitmap();


    }
}

package com.softdragon.omniwatch.library.view.element.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Environment;
import android.support.v4.content.ContextCompat;

/**
 * Created by Dawid on 2016-02-07.
 */
public class BitmapPartUrl extends BitmapPart<String> {

    public BitmapPartUrl(String... bitmapRes) {
        super(bitmapRes);
    }

    @Override
    protected Bitmap loadBitmap(String res) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        String url = Environment.getExternalStorageDirectory() + "/" + res;
        return BitmapFactory.decodeFile(url, options);

    }
}

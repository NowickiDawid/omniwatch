package com.softdragon.omniwatch.library.view.element.utils;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.util.Log;

import java.util.Arrays;

/**
 * Created by Dawid on 2016-02-10.
 */
public class ColorFilterGenerator {
    public static ColorFilter adjustColor(float saturation, float hue) {
        saturation = cleanValue(saturation, 1);
        hue = cleanValue(hue, 360);

        int srcColor = Color.HSVToColor(new float[]{hue, saturation,  1});
        PorterDuff.Mode mode = PorterDuff.Mode.MULTIPLY ;

        PorterDuffColorFilter porterDuffColorFilter
                = new PorterDuffColorFilter(srcColor, mode);
        return porterDuffColorFilter;
    }
    public static ColorFilter adjustBlack() {
        int srcColor = Color.HSVToColor(new float[]{1, 360,  0});
        PorterDuff.Mode mode = PorterDuff.Mode.MULTIPLY ;

        PorterDuffColorFilter porterDuffColorFilter
                = new PorterDuffColorFilter(srcColor, mode);
        return porterDuffColorFilter;
    }
    protected static float cleanValue(float p_val, float p_limit) {
        return Math.min(p_limit, Math.max(-p_limit, p_val));
    }
}

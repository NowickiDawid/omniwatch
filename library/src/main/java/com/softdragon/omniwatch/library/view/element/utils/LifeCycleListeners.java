package com.softdragon.omniwatch.library.view.element.utils;

import android.content.Context;

/**
 * Created by Dawid on 2016-02-07.
 */
public interface LifeCycleListeners {
    void onCreate(Context context);

    void onDestroy(Context context);
}

package com.softdragon.omniwatch.library.view.element.utils;

import android.util.Log;

/**
 * Created by Dawid on 2016-03-06.
 */
public class TimeCounter {
    private static final String TAG = "TimeCounter";
    private static long mStartTime;
    private final String mName;
    float mAVGTimeOfDraw = 0;
    boolean mFirstTime = true;

    public TimeCounter(String name) {
        mName = name;
        reset();
    }

    public void start() {
        mStartTime = System.currentTimeMillis();
    }

    public void end() {
        countAVGOfDraw(System.currentTimeMillis() - mStartTime);
    }

    public void reset() {
        mAVGTimeOfDraw = 0;
        mFirstTime = true;
    }

    private void countAVGOfDraw(long time) {
        if (mFirstTime) {
            countForFirstValue(time);
            return;
        }
        countAvg(time);
    }

    private void countAvg(long time) {
        float alfa = 0.1f;
        mAVGTimeOfDraw = time * alfa + (1 - alfa) * mAVGTimeOfDraw;
        log(time);
    }

    private void log(long time) {
        Log.d(TAG, String.format(mName + " time=%f last time=%d", mAVGTimeOfDraw, time));
    }

    private void countForFirstValue(long time) {
        mAVGTimeOfDraw = time;
        mFirstTime = false;
        log(time);
    }


}

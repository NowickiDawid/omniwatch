package com.softdragon.omniwatch.library.viewmodel;


import android.os.Handler;
import android.os.Message;

import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.timemode.TimeModeService;
import com.softdragon.omniwatch.library.timemode.TimeModeService.TimeMode;
import com.softdragon.omniwatch.library.view.OmniWatchView;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.TimeZone;


/**
 * Created by Dawid on 2016-02-06.
 */
public class OmniWatchViewModel {
    /**
     * Update rate in milliseconds for interactive mode. We update once a second to advance the
     * second hand.
     */

    /**
     * Handler message id for updating the time periodically in interactive mode.
     */

    private static final int MSG_UPDATE_TIME = 0;
    private TimeMode mTimeMode = new TimeModeService().createTimeMode();
    private long INTERACTIVE_UPDATE_RATE_MS = mTimeMode.getStandardModeTimerRate();
    private long INTERACTIVE_AMBITION_UPDATE_RATE_MS = mTimeMode.getAmbitionModeTimerRate();
    private Params mParams;
    private OmniWatchView mView;
    private boolean mLowBitAmbientMode;
    private boolean mAmbientMode;
    final Handler mUpdateTimeHandler = new EngineHandler(this);
    private int mBottomChin;
    private boolean mIsScreenRound;
    private boolean mIsWatchScreenParamsSetted;
    private long interaction = INTERACTIVE_AMBITION_UPDATE_RATE_MS;

    public OmniWatchViewModel() {
        mParams = new Params();
    }


    private static class EngineHandler extends Handler {
        private final WeakReference<OmniWatchViewModel> mWeakReference;

        public EngineHandler(OmniWatchViewModel reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            OmniWatchViewModel engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    public void onCreate() {
        mTimeMode.createCalendar();

    }

    public void onDestroy() {
        removeTimer();
    }

    public void changeTimeZone() {
        mTimeMode.setTimeZone(TimeZone.getDefault());
    }


    public void onTouchScreen(int x, int y, long eventTime) {

    }

    public void onTouchCancel(int x, int y, long eventTime) {

    }

    public void onTap(int x, int y, long eventTime) {

    }

    public Params getParams() {
        return mParams;
    }

    public void setParams(Params params) {
        mParams = params;
    }


    public void setLowBitAmbientMode(boolean lowBitAmbientMode) {
        mLowBitAmbientMode = lowBitAmbientMode;
    }

    public boolean isLowBitAmbientMode() {
        return mLowBitAmbientMode;
    }

    public boolean isAmbientMode() {
        return mAmbientMode;
    }

    public void setAmbientMode(boolean inAmbientMode) {
        mAmbientMode = inAmbientMode;
        interaction = mAmbientMode ? INTERACTIVE_AMBITION_UPDATE_RATE_MS : INTERACTIVE_UPDATE_RATE_MS;
        updateTimer();
    }

    public void onDraw() {
        mTimeMode.refreshTime();
    }

    public void setView(OmniWatchView view) {
        mView = view;
    }

    public void setBottomChin(int bottomChin) {
        mBottomChin = bottomChin;
    }

    public void setIsScreenRound(boolean isScreenRound) {
        mIsScreenRound = isScreenRound;
    }

    public int getBottomChin() {
        return mBottomChin;
    }

    public boolean isScreenRound() {
        return mIsScreenRound;
    }

    public boolean isWatchScreenParamsSetted() {
        return mIsWatchScreenParamsSetted;
    }

    public void setWatchScreenParamsSetted(boolean watchScreenParamsSetted) {
        mIsWatchScreenParamsSetted = watchScreenParamsSetted;
    }

    public void setIsWatchScreenParamsSetted(boolean isWatchScreenParamsSetted) {
        mIsWatchScreenParamsSetted = isWatchScreenParamsSetted;
    }

    public Calendar getCalendar() {
        return mTimeMode.getCalendar();
    }

    /**
     * Starts the {@link #mUpdateTimeHandler} timer if it should be running and isn't currently
     * or stops it if it shouldn't be running but currently is.
     */

    private void handleUpdateTimeMessage() {
        mView.invalidateAll();
        if (mView.shouldTimerBeRunning()) {
            long timeMs = System.currentTimeMillis();
            long delayMs = interaction
                    - (timeMs % interaction);
            mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
        }
    }

    public void removeTimer() {
        mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
    }

    public void updateTimer() {
        mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
        if (mView.shouldTimerBeRunning()) {
            mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
        }
    }

}

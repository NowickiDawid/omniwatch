package com.softdragon.omniwatch.library.presenter;

import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.R;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.utils.TextUtils;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.group.BitmapGroupElement;
import com.softdragon.omniwatch.library.view.element.MultiBitmapMultiStateGraphicElement;
import com.softdragon.omniwatch.library.view.element.OneBitmapMultiStateGraphicElement;
import com.softdragon.omniwatch.library.view.element.SimpleBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleRoundeBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleSquareBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.battery.BatteryLevelGraphicDescription;
import com.softdragon.omniwatch.library.view.element.counter.CounterGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.SimpleTextGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.tips.CustomDrawableTipsGraphicElement;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPartResource;
import com.softdragon.omniwatch.library.view.element.utils.ColorFilterGenerator;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dawid on 2016-02-11.
 */
public class WatchFace extends WatchFaceBaseImp {
    private BackgroundGraphicElement mBackground;
    private Paint mPaint;
    private Paint mPaintText;

    private Paint mPaintTextDescription;
    private BatteryLevelGraphicDescription batteryLevelGraphicDescription;
    private Paint mPainAmbition;
    private WatchParamAdapterImp mWatchParamsAdapterImp;

    public WatchFace(OmniWatchView omniWatchView) {
        super(omniWatchView);
    }

    @Override
    public BackgroundGraphicElement createRoundBackground() {
        return createBackground(R.mipmap.background);
    }

    @Override
    public BackgroundGraphicElement createSquareBackground() {
        return createBackground(R.mipmap.background_square);
    }

    @Override
    public List<GraphicElement> createRoundNormalMode() {
        return createMode(createMinuteBackground(R.mipmap.nexo_paski));
    }

    @Override
    public List<GraphicElement> createSquareNormalMode() {
        List<GraphicElement> list = new ArrayList<>();
        list.add(new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
            }

            @Override
            public boolean isNeedRedraw() {
                return mOmniWatchView.isNeedRedraw();
            }

            @Override
            public List<GraphicElement> getGraphicElement() {
                List<GraphicElement> elements = new ArrayList<>();
                createBackground(elements);
                elements.add(new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
                    @Override
                    public BitmapPart getmBitmapPart() {
                        return new BitmapPartResource(R.mipmap.nexo_paski);
                    }

                    @Override
                    public Paint createPaint() {
                        return mPaint;
                    }
                }));
                elements.add(createMinuteBackground(R.mipmap.nexo_paski_square_paski));
                createFrontFace(elements);
                return elements;
            }
        }));
        list.add(new SimpleTextGraphicElement(new DateTextGraphicDescription() {

            @Override
            protected int getField() {
                return Calendar.SECOND;
            }

            @Override
            public float getPercentX() {
                return 0.81f;
            }

            @Override
            public float getPercentY() {
                return 0.56f;
            }


        }));
        return list;
    }

    private void createBackground(List<GraphicElement> elements) {
        elements.add(mBackground);
        elements.add(new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
            @Override
            public String getText() {
                return TextUtils.getCapFirst(mOmniWatchView.getCalendar().getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH));
            }


            @Override
            public float getPercentX() {
                return 0.19f;
            }

            @Override
            public float getPercentY() {
                return 0.5f;
            }
        }));
    }

    private void createFrontFace(List<GraphicElement> elements) {

        elements.add(new SimpleTextGraphicElement(new DateTextGraphicDescription() {


            @Override
            protected int getField() {
                return Calendar.DAY_OF_MONTH;
            }

            @Override
            public float getPercentX() {
                return 0.19f;
            }

            @Override
            public float getPercentY() {
                return 0.56f;
            }
        }));
        elements.add(new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
            @Override
            public String getText() {
                return "Sec";
            }

            @Override
            public float getPercentX() {
                return 0.81f;
            }

            @Override
            public float getPercentY() {
                return 0.50f;
            }

        }));
        elements.add(new CounterGraphicElement(new MultiBitmapMultiStateGraphicElement.MultiBitmapMultiStateGraphicsDescription() {


            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
            }

            @Override
            public int getSize() {
                return 2;
            }

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(
                        R.mipmap.number_0,
                        R.mipmap.number_1,
                        R.mipmap.number_2,
                        R.mipmap.number_3,
                        R.mipmap.number_4,
                        R.mipmap.number_5,
                        R.mipmap.number_6,
                        R.mipmap.number_7,
                        R.mipmap.number_8,
                        R.mipmap.number_9);

            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.5f;
            }
        }));
        elements.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.days);
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.29f;
            }
        }));
        elements.add(new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.29f;
            }


            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.DAY_OF_WEEK) - 1;
            }

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(
                        R.mipmap.day_7,
                        R.mipmap.day_1,
                        R.mipmap.day_2,
                        R.mipmap.day_3,
                        R.mipmap.day_4,
                        R.mipmap.day_5,
                        R.mipmap.day_6);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }
        }));
        elements.add(new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.68f;
            }


            @Override
            public int getValue() {
                return mOmniWatchView.is12Hour() ? mOmniWatchView.getCalendar().get(Calendar.AM_PM) : -1;
            }

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(
                        R.mipmap.am,
                        R.mipmap.pm);
            }

            @Override
            public Paint createPaint() {
                return null;
            }
        }));
        elements.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.bateria_symbol);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return 0.38f;
            }

            @Override
            public float getPercentY() {
                return 0.7f;
            }
        }));
        elements.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.b_d);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.73f;
            }

        }));
        elements.add(new OneBitmapMultiStateGraphicElement(batteryLevelGraphicDescription));
        elements.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.b_g);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.73f;
            }
        }));
        elements.add(new CustomDrawableTipsGraphicElement(new CustomDrawableTipsGraphicElement.CustomDrawerTipsDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(
                        R.mipmap.tip_1,
                        R.mipmap.tip_2);
            }

            @Override
            public float getLocation() {
                return 0.84f;
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }
        }));
    }

    @Override
    public List<GraphicElement> createSquareAmbitionMode() {
        return createMinuteAmbition(new SimpleSquareBackgroundGraphicElement(Color.BLACK));
    }

    @Override
    public List<GraphicElement> createRoundAmbitionMode() {
        return createMinuteAmbition(new SimpleRoundeBackgroundGraphicElement(Color.BLACK));
    }

    @Override
    public void createPaint() {
        mPaint = mOmniWatchView.createStandardPaint();
        mPaintText = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPaintTextDescription = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPainAmbition = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Thin.ttf"));
        mWatchParamsAdapterImp = new WatchParamAdapterImp(mOmniWatchView.getContext());
    }

    @NonNull
    private BackgroundGraphicElement createBackground(final int backgroundRes) {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(backgroundRes);
            }

            @Override
            public Paint createPaint() {
                return null;
            }
        });
        return mBackground;
    }

    @Override
    protected Paint createTextPaint(int color, Typeface typeface) {
        Paint paint = super.createTextPaint(color, typeface);
        paint.setTextSize(10);
        return paint;
    }

    @Override
    public void setParams(Params params) {
        ColorFilter filter = getColorFilter(params, mWatchParamsAdapterImp.getAdapter());
        mPaint.setColorFilter(filter);
        mPaintTextDescription.setColorFilter(filter);
        mPainAmbition.setColorFilter(filter);
        mOmniWatchView.set12Hour(mWatchParamsAdapterImp.getAdapter().get12Hour(params));
    }

    @Override
    public List<LifeCycleListeners> createLiveCycleListeners() {
        batteryLevelGraphicDescription = new BatteryLevelGraphicDescription(9) {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(
                        R.mipmap.b_10,
                        R.mipmap.b_20,
                        R.mipmap.b_30,
                        R.mipmap.b_40,
                        R.mipmap.b_50,
                        R.mipmap.b_60,
                        R.mipmap.b_70,
                        R.mipmap.b_80,
                        R.mipmap.b_90,
                        R.mipmap.b_100);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.73f;
            }


        };
        List<LifeCycleListeners> lifeCycleListeners = new ArrayList<>();
        lifeCycleListeners.add(batteryLevelGraphicDescription);
        return lifeCycleListeners;
    }

    @NonNull
    private List<GraphicElement> createMode(final SimpleBitmapGraphicElement minuteBackground) {
        List<GraphicElement> list = new ArrayList<>();
        list.add(new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
            }

            @Override
            public boolean isNeedRedraw() {
                return mOmniWatchView.isNeedRedraw();
            }

            @Override
            public List<GraphicElement> getGraphicElement() {
                List<GraphicElement> elements = new ArrayList<>();
                elements.add(mBackground);
                elements.add(new SimpleTextGraphicElement(new SimpleTextGraphicDescription() {
                    @Override
                    public String getText() {
                        return TextUtils.getCapFirst(mOmniWatchView.getCalendar().getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH));
                    }


                    @Override
                    public float getPercentX() {
                        return 0.19f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.5f;
                    }
                }));
                elements.add(minuteBackground);
                createFrontFace(elements);
                return elements;

            }
        }));
        list.add(new SimpleTextGraphicElement(new DateTextGraphicDescription() {

            @Override
            protected int getField() {
                return Calendar.SECOND;
            }

            @Override
            public float getPercentX() {
                return 0.81f;
            }

            @Override
            public float getPercentY() {
                return 0.56f;
            }


        }));
        return list;
    }

    @NonNull
    private List<GraphicElement> createMinuteAmbition(final GraphicElement background) {
        final List<GraphicElement> list = new ArrayList<>();
        list.add(new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.HOUR_OF_DAY);
            }

            @Override
            public boolean isNeedRedraw() {
                return mOmniWatchView.isNeedRedraw();
            }

            @Override
            public List<GraphicElement> getGraphicElement() {
                List<GraphicElement> elements = new ArrayList<>();
                elements.add(background);
                elements.add(new OneBitmapMultiStateGraphicElement(new OneBitmapMultiStateGraphicElement.OneBitmapMultiStateGraphicsDescription() {
                    @Override
                    public float getPercentX() {
                        return 0.5f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.5f;
                    }

                    @Override
                    public int getValue() {
                        return mOmniWatchView.getCalendar().get(Calendar.HOUR);
                    }

                    @Override
                    public BitmapPart createBitmapPart() {
                        return new BitmapPartResource(
                                R.mipmap.h_12,
                                R.mipmap.h_13,
                                R.mipmap.h_14,
                                R.mipmap.h_15,
                                R.mipmap.h_16,
                                R.mipmap.h_17,
                                R.mipmap.h_18,
                                R.mipmap.h_19,
                                R.mipmap.h_20,
                                R.mipmap.h_21,
                                R.mipmap.h_22,
                                R.mipmap.h_23) {
                        };
                    }

                    @Override
                    public Paint createPaint() {
                        return mPaint;
                    }
                }));
                return elements;
            }
        }));
        list.add(new SimpleTextGraphicElement(new DateTextGraphicDescription() {

            @Override
            protected int getField() {
                return Calendar.MINUTE;
            }

            @Override
            public Paint getPaint() {
                return mPainAmbition;
            }

            @Override
            public float getTextSize() {
                return mOmniWatchView.getSizeFromResources(R.dimen.main_text_in_ambition);
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.5f;
            }


        }));
        return list;
    }

    @NonNull
    private SimpleBitmapGraphicElement createMinuteBackground(final int backgroundMinute) {
        return new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {

            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(backgroundMinute);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return 0.5f;
            }

            @Override
            public float getPercentY() {
                return 0.5f;
            }
        });
    }

    abstract class SimpleTextGraphicDescription implements TextGraphicDescription {

        @Override
        public Paint getPaint() {
            return mPaintText;
        }

        @Override
        public float getTextSize() {
            return mOmniWatchView.getSizeFromResources(R.dimen.text_simple);
        }
    }

    abstract class DateTextGraphicDescription implements TextGraphicDescription {
        @Override
        public Paint getPaint() {
            return mPaintTextDescription;
        }

        @Override
        public float getTextSize() {
            return mOmniWatchView.getSizeFromResources(R.dimen.text_size);
        }


        @Override
        public String getText() {
            return getDataText(getField());
        }

        protected abstract int getField();
    }

}


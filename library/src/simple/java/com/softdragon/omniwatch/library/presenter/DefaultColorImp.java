package com.softdragon.omniwatch.library.presenter;

/**
 * Created by Dawid on 2016-03-14.
 */
public class DefaultColorImp implements DefaultColor {

    @Override
    public float getDefaultHua() {
        return 131f;
    }

    @Override
    public float getDefaultSaturation() {
        return 78f;
    }
}

package com.softdragon.omniwatch.library.presenter;

import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.R;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.SimpleBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleRoundeBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleSquareBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.clock.ClockBitmapGraphicDescription;
import com.softdragon.omniwatch.library.view.element.clock.ClockBitmapGraphicSetter;
import com.softdragon.omniwatch.library.view.element.group.BitmapGroupElement;
import com.softdragon.omniwatch.library.view.element.text.SimpleTextGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPartResource;
import com.softdragon.omniwatch.library.view.element.utils.ColorFilterGenerator;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Dawid on 2016-02-11.
 */
public class WatchFace extends WatchFaceBaseImp {
    private static final float CENTER_X = 0.50f;
    private static final float CENTER_Y = 0.50f;
    private static final float CENTER_OF_CHINK_Y = CENTER_Y + 0.02f;
    private static final float CENTER_OF_CHINK_SECONDS_Y = CENTER_Y + 0.1f;
    private static final float CENTER_OF_CHINK_SECONDS_X = CENTER_X;
    private BackgroundGraphicElement mBackground;
    private Paint mPaint;
    private Paint mPaintDay;
    private Paint mPaintSeconds;
    private WatchParamAdapterImp mWatchParamsAdapterImp;

    public WatchFace(OmniWatchView omniWatchView) {
        super(omniWatchView);
    }

    @Override
    public void createPaint() {
        mPaint = mOmniWatchView.createStandardPaint();
        mPaintSeconds = mOmniWatchView.createStandardPaint();
        mPaintDay = createTextPaint(Color.WHITE, mOmniWatchView.getFont("fonts/Roboto-Regular.ttf"));
        mWatchParamsAdapterImp = new WatchParamAdapterImp(mOmniWatchView.getContext());

    }

    @Override
    public BackgroundGraphicElement createRoundBackground() {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {

            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.background);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }
        });
        return mBackground;
    }

    @Override
    public BackgroundGraphicElement createSquareBackground() {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {

            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.square_background);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }
        });
        return mBackground;
    }

    @Override
    public List<GraphicElement> createRoundNormalMode() {
        return createModeGraphics(new BitmapPartResource(R.mipmap.background_index));

    }


    @Override
    public List<GraphicElement> createRoundAmbitionMode() {
        ArrayList<GraphicElement> list = new ArrayList<>();
        list.add(new SimpleRoundeBackgroundGraphicElement(mOmniWatchView.getResources().getColor(android.R.color.black)));
        createAmbition(new BitmapPartResource(R.mipmap.background_index), list);
        return list;
    }

    @NonNull
    private ArrayList<GraphicElement> createAmbition(BitmapPartResource bitmapPartResource, ArrayList<GraphicElement> list) {
        final ClockBitmapGraphicDescription clockBitmapGraphicDescription = createNotClockBitmap();
        addIndexBackground(list, bitmapPartResource);
        list.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockBitmapGraphicDescription));
        addAmbentScrew(list);
        return list;
    }

    @Override
    public List<GraphicElement> createSquareNormalMode() {
        return createModeGraphics(new BitmapPartResource(R.mipmap.square_background_index));

    }

    private ArrayList<GraphicElement> createModeGraphics(final BitmapPart bitmapPart) {
        ArrayList<GraphicElement> list = new ArrayList<>();
        final ClockBitmapGraphicDescription clockBitmapGraphicDescription = createNotClockBitmap();
        final ClockBitmapGraphicDescription colorClockBitmapGraphicDescription = createColorClockBitmap();
        list.add(new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
            }

            @Override
            public boolean isNeedRedraw() {
                return mOmniWatchView.isNeedRedraw();
            }

            @Override
            public List<GraphicElement> getGraphicElement() {
                List<GraphicElement> listGraphicElement = new ArrayList<>();
                listGraphicElement.add(mBackground);
                addIndexBackground(listGraphicElement, bitmapPart);
                addDayMonth(listGraphicElement);
                addLogo(listGraphicElement);
                listGraphicElement.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockBitmapGraphicDescription));
                listGraphicElement.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockBitmapGraphicDescription));
                return listGraphicElement;
            }
        }));

        list.add(ClockBitmapGraphicSetter.createSecondGraphicElement(colorClockBitmapGraphicDescription));
        addScrew(list);
        return list;

    }

    private void addDayMonth(List<GraphicElement> listGraphicElement) {
        listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {

            @Override
            public Paint getPaint() {
                return mPaintDay;
            }

            @Override
            public String getText() {
                return getDataText(Calendar.DAY_OF_MONTH);
            }

            @Override
            public float getTextSize() {
                return mOmniWatchView.getSizeFromResources(R.dimen.text_day_of_month);

            }

            @Override
            public float getPercentX() {
                return 0.725f;
            }

            @Override
            public float getPercentY() {
                return 0.50f;
            }
        }));
    }

    private void addScrew(ArrayList<GraphicElement> list) {
        list.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.screw);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
    }

    private void addAmbentScrew(ArrayList<GraphicElement> list) {
        list.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.ambient_screw);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
    }

    private void addIndexBackground(List<GraphicElement> listGraphicElement, final BitmapPart bitmapPart) {
        listGraphicElement.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return bitmapPart;
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
    }

    private void addLogo(List<GraphicElement> listGraphicElement) {
        listGraphicElement.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.logo);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return 0.50f;
            }

            @Override
            public float getPercentY() {
                return 0.75f;
            }
        }));
    }


    private ClockBitmapGraphicDescription createNotClockBitmap() {
        return new ClockBitmapGraphicDescriptionImpl() {

            @Override
            public BitmapPart createHourBitmapPart() {
                return new BitmapPartResource(R.mipmap.hours);
            }

            @Override
            public Paint getClockPaint() {
                return null;
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return new BitmapPartResource(R.mipmap.minutes);
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return null;
            }
        };
    }

    private ClockBitmapGraphicDescription createColorClockBitmap() {
        return new ClockBitmapGraphicDescriptionImpl() {

            @Override
            public BitmapPart createHourBitmapPart() {
                return null;
            }

            @Override
            public Paint getClockPaint() {
                return mPaintSeconds;
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return null;
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return new BitmapPartResource(R.mipmap.seconds);
            }
        };
    }

    @Override
    public List<GraphicElement> createSquareAmbitionMode() {
        ArrayList<GraphicElement> list = new ArrayList<>();
        list.add(new SimpleSquareBackgroundGraphicElement(mOmniWatchView.getResources().getColor(android.R.color.black)));
        createAmbition(new BitmapPartResource(R.mipmap.square_background_index), list);
        return list;
    }

    @Override
    public List<LifeCycleListeners> createLiveCycleListeners() {
        return new ArrayList<>();
    }

    @Override
    public void setParams(Params params) {
        setColorsWithBlack(mPaint, params, mWatchParamsAdapterImp.getGlobal());
        setColorsWithBlack(mPaintSeconds, params, mWatchParamsAdapterImp.getSeconds());
        mOmniWatchView.set12Hour(mWatchParamsAdapterImp.getGlobal().get12Hour(params));
    }

    private void setColorsWithBlack(Paint paint, Params params, ColorParamsAdapter adapter) {
        if (adapter.getBlackMode(params))
            paint.setColorFilter(ColorFilterGenerator.adjustBlack());
        else
            setColors(paint, params, adapter);
    }


    abstract class ClockBitmapGraphicDescriptionImpl extends ClockBitmapGraphicDescription {

        @Override
        public Calendar getCalendar() {
            return mOmniWatchView.getCalendar();
        }

        @Override
        public float getPercentChinY() {
            return CENTER_OF_CHINK_Y;
        }

        @Override
        public float getPercentSecondChinX() {
            return CENTER_OF_CHINK_SECONDS_X;
        }

        @Override
        public float getPercentSecondChinY() {
            return CENTER_OF_CHINK_SECONDS_Y;
        }

        @Override
        public float getPercentX() {
            return CENTER_X;
        }

        @Override
        public float getPercentY() {
            return CENTER_Y;
        }
    }
}
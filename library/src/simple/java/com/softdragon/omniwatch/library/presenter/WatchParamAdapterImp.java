package com.softdragon.omniwatch.library.presenter;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.wearable.DataMap;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.presenter.params.WatchParamAdapter;
import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-07-11.
 */
public class WatchParamAdapterImp extends WatchParamAdapter {
    private ColorParamsAdapter mGlobal;
    private ColorParamsAdapter mSeconds;

    public WatchParamAdapterImp(Context context) {
        mGlobal = new ColorParamsAdapter(context);
        mSeconds = new ColorParamsAdapter(context, "seconds", new DefaultColor() {
            @Override
            public float getDefaultHua() {
                return 57;
            }

            @Override
            public float getDefaultSaturation() {
                return 85;
            }
        });
        mGlobal.setIs12Able(false);
        mSeconds.setIs12Able(false);
        mGlobal.setIsBlackAble(true);
        mSeconds.setIsBlackAble(true);
    }

    public ColorParamsAdapter getGlobal() {
        return mGlobal;
    }

    public ColorParamsAdapter getSeconds() {
        return mSeconds;
    }


    @Override
    public void save(DataMap dataMap, StoragePreference storagePreference) {
        mGlobal.save(dataMap, storagePreference);
        mSeconds.save(dataMap, storagePreference);
    }

    @Override
    public void save(Params dataMap, StoragePreference storagePreference) {
        mGlobal.save(dataMap, storagePreference);
        mSeconds.save(dataMap, storagePreference);
    }

    @Override
    public Params load(StoragePreference storage) {
        Bundle data = new Bundle();
        mGlobal.load(storage, data);
        mSeconds.load(storage, data);
        return mGlobal.createParams(data);
    }

    @Override
    public DataMap getData(Params params) {
        DataMap dataMap = new DataMap();
        mGlobal.getData(params, dataMap);
        mSeconds.getData(params, dataMap);
        return dataMap;
    }

    @Override
    public ColorParamsAdapter getAdapter() {
        return mGlobal;
    }

    @Override
    public ColorParamsAdapter getColorParamsAdapter(int index) {
        if (index == 0)
            return mGlobal;
        else
            return mSeconds;
    }
}

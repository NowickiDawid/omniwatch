package com.softdragon.omniwatch.library.presenter;

import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;

import com.softdragon.omniwatch.library.R;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.utils.TextUtils;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.view.element.BackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.GraphicElement;
import com.softdragon.omniwatch.library.view.element.RotatingBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.SimpleBitmapGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleRoundeBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.background.SimpleSquareBackgroundGraphicElement;
import com.softdragon.omniwatch.library.view.element.battery.BatteryLevelGraphicDescription;
import com.softdragon.omniwatch.library.view.element.clock.ClockBitmapGraphicDescription;
import com.softdragon.omniwatch.library.view.element.clock.ClockBitmapGraphicSetter;
import com.softdragon.omniwatch.library.view.element.group.BitmapGroupElement;
import com.softdragon.omniwatch.library.view.element.text.SimpleTextGraphicElement;
import com.softdragon.omniwatch.library.view.element.text.description.TextGraphicDescription;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPart;
import com.softdragon.omniwatch.library.view.element.utils.BitmapPartResource;
import com.softdragon.omniwatch.library.view.element.utils.ColorFilterGenerator;
import com.softdragon.omniwatch.library.view.element.utils.LifeCycleListeners;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Dawid on 2016-02-11.
 */
public class WatchFace extends WatchFaceBaseImp {
    private static final float CENTER_X = 0.50f;
    private static final float CENTER_Y = 0.50f;
    private static final float BATTERY_X = 0.28f;
    private static final float CENTER_OF_CHINK_Y = CENTER_Y + 0.08f;
    private static final float CENTER_OF_CHINK_SECONDS_Y = CENTER_Y + 0.08f;
    private static final float BATTERY_Y = 0.53f;
    private static final float CENTER_OF_CHINK_SECONDS_X = 0.498f;
    private Paint mPaint;
    private RotateBatteryLevelGraphicDescription mRotateBatteryLevelGraphicDescription;
    private RotateBatteryLevelGraphicDescription mRotateColorBatteryLevelGraphicDescription;
    private Paint mPaintTextHours;
    private Paint mPaintTextMinutes;
    private Paint mPaintTextDate;
    private Paint mPaintTextDayOfWeek;
    private BackgroundGraphicElement mBackground;
    private Paint mPaintTextPMAM;
    private WatchParamAdapterImp mWatchParamAdapterImp;


    public WatchFace(OmniWatchView omniWatchView) {
        super(omniWatchView);
    }


    @Override
    public void createPaint() {
        mPaint = mOmniWatchView.createStandardPaint();
        mPaintTextHours = createTextPaint(getColor(R.color.hours), mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPaintTextMinutes = createTextPaint(getColor(R.color.minutes), mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPaintTextPMAM = createTextPaint(getColor(R.color.minutes), mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPaintTextDate = createTextPaint(getColor(R.color.day_of_month), mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mPaintTextDayOfWeek = createTextPaint(getColor(R.color.data), mOmniWatchView.getFont("fonts/Roboto-Light.ttf"));
        mWatchParamAdapterImp = new WatchParamAdapterImp(mOmniWatchView.getContext());

    }

    @Override
    public BackgroundGraphicElement createRoundBackground() {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.background);
            }

            @Override
            public Paint createPaint() {
                return null;
            }
        });
        return mBackground;
    }

    @Override
    public BackgroundGraphicElement createSquareBackground() {
        mBackground = new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.square_background);
            }

            @Override
            public Paint createPaint() {
                return null;
            }
        });
        return mBackground;
    }


    @Override
    public List<GraphicElement> createRoundNormalMode() {
        return createModeGraphics(new BitmapPartResource(R.mipmap.background_color));
    }

    @Override
    public List<GraphicElement> createSquareNormalMode() {
        return createModeGraphics(new BitmapPartResource(R.mipmap.square_background_color));
    }


    private ArrayList<GraphicElement> createModeGraphics(final BitmapPart colorGraphics) {
        ArrayList<GraphicElement> list = new ArrayList<>();
        final ClockBitmapGraphicDescription clockColorBitmapGraphicDescription = createColorClockBitmap();
        final ClockBitmapGraphicDescription clockBitmapGraphicDescription = createNotClockBitmap();
        list.add(new BitmapGroupElement(new BitmapGroupElement.BitmapGroupDescription() {
            @Override
            public int getValue() {
                return mOmniWatchView.getCalendar().get(Calendar.MINUTE);
            }

            @Override
            public boolean isNeedRedraw() {
                return mOmniWatchView.isNeedRedraw();
            }

            @Override
            public List<GraphicElement> getGraphicElement() {
                List<GraphicElement> listGraphicElement = new ArrayList<GraphicElement>();
                listGraphicElement.add(mBackground);
                listGraphicElement.add(new SimpleTextGraphicElement(new TextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintTextDayOfWeek;
                    }

                    @Override
                    public String getText() {
                        return TextUtils.getCaptionText(mOmniWatchView.getCalendar().getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.ENGLISH));
                    }

                    @Override
                    public float getPercentX() {
                        return 0.5f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.28f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_size_description);
                    }

                }));
                listGraphicElement.add(new SimpleTextGraphicElement(new TimeDateTextGraphicDescription() {
                    @Override
                    public Paint getPaint() {
                        return mPaintTextDate;
                    }

                    @Override
                    protected int getField() {
                        return Calendar.DAY_OF_MONTH;
                    }

                    @Override
                    public float getPercentX() {
                        return 0.5f;
                    }

                    @Override
                    public float getPercentY() {
                        return 0.34f;
                    }

                    @Override
                    public float getTextSize() {
                        return mOmniWatchView.getSizeFromResources(R.dimen.text_day_of_month);
                    }


                }));
                addMinuteAndHoursText(listGraphicElement);
                listGraphicElement.add(new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {
                    @Override
                    public BitmapPart getmBitmapPart() {
                        return colorGraphics;
                    }

                    @Override
                    public Paint createPaint() {
                        return mPaint;
                    }
                }));

                listGraphicElement.add(new RotatingBitmapGraphicElement(mRotateBatteryLevelGraphicDescription));
                listGraphicElement.add(new RotatingBitmapGraphicElement(mRotateColorBatteryLevelGraphicDescription));
                addMinuteAndHours(clockColorBitmapGraphicDescription, clockBitmapGraphicDescription, listGraphicElement);
                return listGraphicElement;
            }


        }));
        list.add(ClockBitmapGraphicSetter.createSecondGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.createSecondGraphicElement(clockColorBitmapGraphicDescription));
        list.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.screw);
            }

            @Override
            public Paint createPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
        return list;
    }


    @Override
    public List<GraphicElement> createRoundAmbitionMode() {
        List<GraphicElement> list = new ArrayList<>();
        list.add(new SimpleRoundeBackgroundGraphicElement(mOmniWatchView.getResources().getColor(android.R.color.black)));
        list.add(new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {

            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.ambient_mode);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

        }));
        addRestToAmbientMode(list);
        return list;
    }

    private void addRestToAmbientMode(List<GraphicElement> list) {
        ClockBitmapGraphicDescription clockColorBitmapGraphicDescription = createColorClockBitmap();
        ClockBitmapGraphicDescription clockBitmapGraphicDescription = createAmbientNotClockBitmap();
        addMinuteAndHoursText(list);
        addMinuteAndHours(clockColorBitmapGraphicDescription, clockBitmapGraphicDescription, list);
        list.add(new SimpleBitmapGraphicElement(new SimpleBitmapGraphicElement.SimpleBitmapGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(R.mipmap.screw_ambient_mode_color);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        }));
    }

    private void addMinuteAndHoursText(List<GraphicElement> list) {
        list.add(createHourText());
        list.add(createMinuteText());
        list.add(createAMPM());
    }


    private void addMinuteAndHours(ClockBitmapGraphicDescription clockColorBitmapGraphicDescription, ClockBitmapGraphicDescription clockBitmapGraphicDescription, List<GraphicElement> list) {
        list.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.createHourGraphicElement(clockColorBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockBitmapGraphicDescription));
        list.add(ClockBitmapGraphicSetter.creteMinuteGraphicElement(clockColorBitmapGraphicDescription));
    }

    @NonNull
    private ClockBitmapGraphicDescription createColorClockBitmap() {
        return new ClockBitmapGraphicDescription() {
            @Override
            public Calendar getCalendar() {
                return mOmniWatchView.getCalendar();
            }

            @Override
            public float getPercentChinY() {
                return CENTER_OF_CHINK_Y;
            }

            @Override
            public float getPercentSecondChinX() {
                return CENTER_OF_CHINK_SECONDS_X;
            }

            @Override
            public float getPercentSecondChinY() {
                return CENTER_OF_CHINK_SECONDS_Y;
            }

            @Override
            public BitmapPart createHourBitmapPart() {
                return new BitmapPartResource(R.mipmap.hours_color);
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return new BitmapPartResource(R.mipmap.minutes_color);
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return new BitmapPartResource(R.mipmap.seconds_color);
            }

            @Override
            public Paint getClockPaint() {
                return mPaint;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        };
    }

    @NonNull
    private ClockBitmapGraphicDescription createNotClockBitmap() {
        return new ClockBitmapGraphicDescription() {
            @Override
            public Calendar getCalendar() {
                return mOmniWatchView.getCalendar();
            }

            @Override
            public float getPercentChinY() {
                return CENTER_OF_CHINK_Y;
            }

            @Override
            public float getPercentSecondChinX() {
                return CENTER_OF_CHINK_SECONDS_X;
            }

            @Override
            public float getPercentSecondChinY() {
                return CENTER_OF_CHINK_SECONDS_Y;
            }

            @Override
            public BitmapPart createHourBitmapPart() {
                return new BitmapPartResource(R.mipmap.hours);
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return new BitmapPartResource(R.mipmap.minutes);
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return new BitmapPartResource(R.mipmap.seconds);
            }

            @Override
            public Paint getClockPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        };
    }

    @NonNull
    private ClockBitmapGraphicDescription createAmbientNotClockBitmap() {
        return new ClockBitmapGraphicDescription() {
            @Override
            public Calendar getCalendar() {
                return mOmniWatchView.getCalendar();
            }

            @Override
            public float getPercentChinY() {
                return CENTER_OF_CHINK_Y;
            }

            @Override
            public float getPercentSecondChinX() {
                return CENTER_OF_CHINK_SECONDS_X;
            }

            @Override
            public float getPercentSecondChinY() {
                return CENTER_OF_CHINK_SECONDS_Y;
            }

            @Override
            public BitmapPart createHourBitmapPart() {
                return new BitmapPartResource(R.mipmap.hours_ambient);
            }

            @Override
            public BitmapPart createMinuteBitmapPart() {
                return new BitmapPartResource(R.mipmap.minutes_ambient);
            }

            @Override
            public BitmapPart createSecondBitmapPart() {
                return new BitmapPartResource(R.mipmap.seconds);
            }

            @Override
            public Paint getClockPaint() {
                return null;
            }

            @Override
            public float getPercentX() {
                return CENTER_X;
            }

            @Override
            public float getPercentY() {
                return CENTER_Y;
            }
        };
    }

    @NonNull
    private SimpleTextGraphicElement createHourText() {
        return new SimpleTextGraphicElement(new DateTextGraphicDescription() {

            @Override
            public Paint getPaint() {
                return mPaintTextHours;
            }

            @Override
            public String getText() {
                int hour = mOmniWatchView.getCalendar().get(Calendar.HOUR);
                return getTimeToText(mOmniWatchView.is12Hour() ? hour == 0 ? 12 : hour : mOmniWatchView.getCalendar().get(Calendar.HOUR_OF_DAY));
            }

            @Override
            public float getPercentX() {
                return 0.75f;
            }

            @Override
            public float getPercentY() {
                return 0.45f;
            }


        });
    }

    @NonNull
    private SimpleTextGraphicElement createAMPM() {
        return new SimpleTextGraphicElement(new TextGraphicDescription() {
            @Override
            public Paint getPaint() {
                return mPaintTextPMAM;
            }

            @Override
            public String getText() {
                return mOmniWatchView.is12Hour() ? mOmniWatchView.getCalendar().get(Calendar.AM_PM) == 0 ? "AM" : "PM" : "";
            }

            @Override
            public float getTextSize() {
                return mOmniWatchView.getSizeFromResources(R.dimen.text_size_pm_am);
            }


            @Override
            public float getPercentX() {
                return 0.78f;
            }

            @Override
            public float getPercentY() {
                return 0.62f;
            }
        });
    }

    @NonNull
    private SimpleTextGraphicElement createMinuteText() {
        return new SimpleTextGraphicElement(new TimeDateTextGraphicDescription() {

            @Override
            public Paint getPaint() {
                return mPaintTextMinutes;
            }

            @Override
            protected int getField() {
                return Calendar.MINUTE;
            }


            @Override
            public float getPercentX() {
                return 0.75f;
            }

            @Override
            public float getPercentY() {
                return 0.56f;
            }


        });
    }


    @Override
    public List<GraphicElement> createSquareAmbitionMode() {
        List<GraphicElement> list = new ArrayList<>();
        list.add(new SimpleSquareBackgroundGraphicElement(mOmniWatchView.getResources().getColor(android.R.color.black)));
        list.add(new BackgroundGraphicElement(new BackgroundGraphicElement.BackgroundGraphicDescription() {

            @Override
            public BitmapPart getmBitmapPart() {
                return new BitmapPartResource(R.mipmap.square_ambient_mode);
            }

            @Override
            public Paint createPaint() {
                return mPaint;
            }

        }));
        addRestToAmbientMode(list);
        return list;
    }

    @Override
    public List<LifeCycleListeners> createLiveCycleListeners() {
        mRotateBatteryLevelGraphicDescription = createBatteryLevelGraphicDescription(R.mipmap.battery, null);
        mRotateColorBatteryLevelGraphicDescription = createBatteryLevelGraphicDescription(R.mipmap.battery_color, mPaint);
        List<LifeCycleListeners> list = new ArrayList<>();
        list.add(mRotateBatteryLevelGraphicDescription);
        list.add(mRotateColorBatteryLevelGraphicDescription);
        return list;
    }

    @Override
    public void setParams(Params params) {
        ColorFilter filter = getColorFilter(params, mWatchParamAdapterImp.getAdapter());
        mPaint.setColorFilter(filter);
        mOmniWatchView.set12Hour(mWatchParamAdapterImp.getAdapter().get12Hour(params));
    }

    private RotateBatteryLevelGraphicDescription createBatteryLevelGraphicDescription(final int batteryRes, final Paint paint) {
        return new RotateBatteryLevelGraphicDescription() {
            @Override
            public BitmapPart createBitmapPart() {
                return new BitmapPartResource(batteryRes);
            }

            @Override
            public Paint createPaint() {
                return paint;
            }
        };
    }


    abstract class RotateBatteryLevelGraphicDescription extends BatteryLevelGraphicDescription implements RotatingBitmapGraphicElement.RotatingBitmapGraphicDescription {

        public RotateBatteryLevelGraphicDescription() {
            super(15);
        }

        @Override
        public int getMaxDegree() {
            return 384;
        }

        @Override
        public int getMinDegree() {
            return 180;
        }

        @Override
        public int getMaxValue() {
            return 16;
        }

        @Override
        public int getMinValue() {
            return 0;
        }

        @Override
        public float getRotationY() {
            return BATTERY_Y - 0.025f;
        }

        @Override
        public float getRotationX() {
            return BATTERY_X;
        }


        @Override
        public float getPercentX() {
            return BATTERY_X;
        }

        @Override
        public float getPercentY() {
            return BATTERY_Y;
        }

    }

    abstract class TimeDateTextGraphicDescription extends DateTextGraphicDescription {

        @Override
        public String getText() {
            return getDataText(getField());
        }

        protected abstract int getField();

    }

    abstract class DateTextGraphicDescription implements TextGraphicDescription {

        @Override
        public float getTextSize() {
            return mOmniWatchView.getSizeFromResources(R.dimen.text_size);
        }


    }
}
package com.softdragon.omniwatch.library.presenter;

import android.content.Context;
import android.os.Bundle;

import com.google.android.gms.wearable.DataMap;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.presenter.params.WatchParamAdapter;
import com.softdragon.omniwatch.library.storage.StoragePreference;

/**
 * Created by Dawid on 2016-07-11.
 */
public class WatchParamAdapterImp extends OneColorStandardWatchParamAdapter {
    public WatchParamAdapterImp(Context context) {
        super(context);
    }
}
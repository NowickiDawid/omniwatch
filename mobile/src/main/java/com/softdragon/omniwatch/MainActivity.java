package com.softdragon.omniwatch;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.crashlytics.android.Crashlytics;
import com.softdragon.omniwatch.fragment.PartSettingFragment;
import com.softdragon.omniwatch.fragment.SettingFragment;
import com.softdragon.omniwatch.fragment.WatchFaceFragment;
import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.presenter.MobileWatchSettings;
import com.softdragon.omniwatch.viewmodel.MainActivityViewModel;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements SettingFragment.ParamsListener, MainActivityViewModel.OnDeviceSettingsListener ,PartSettingFragment.PartSettingFragmentListener{
    MainActivityViewModel mModel = new MainActivityViewModel(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fabric.with(this, new Crashlytics());
        mModel.onCreate(this);
        if (savedInstanceState == null) {
            setFragments();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mModel.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mModel.onPause();
    }


    private void setFragments() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.watch_container, new WatchFaceFragment())
                .replace(R.id.setting_container, MobileWatchSettings.getSettingFragment())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public Params getParams() {
        return mModel.getParams();
    }

    @Override
    public void setParams(Params params) {
        mModel.setParams(params);
    }

    @Override
    public void refreshParams() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.watch_container);
        if (fragment != null)
            ((WatchFaceFragment) fragment).refreshParams(mModel.getParams());

    }

    @Override
    public void onDeviceSettingChanges(DeviceSettings deviceSettings) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.watch_container);
        if (fragment != null)
            ((WatchFaceFragment) fragment).refreshDeviceSettings(deviceSettings);

    }

    @Override
    public DeviceSettings getDeviceSettings() {
        return mModel.getDeviceSettings();
    }

    @Override
    public ColorParamsAdapter getColorParamsAdapter(int index) {
        return mModel.getColorParamsAdapter(index);
    }
}

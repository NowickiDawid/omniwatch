package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

/**
 * Created by Dawid on 2016-06-15.
 */
public class CheckBoxBinding {
    @BindingAdapter("onCheckedChanged")
    public static void setOnCheckedChanged(CheckBox box, CompoundButton.OnCheckedChangeListener listener) {
        box.setOnCheckedChangeListener(listener);
    }
}

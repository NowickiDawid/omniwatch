package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;

import com.softdragon.omniwatch.view.ColorPickerView;

/**
 * Created by Dawid on 2016-03-06.
 */
public class ColorPickerBinding {
    @BindingAdapter("color")
    public static void setColor(ColorPickerView view, int color) {
        view.setColor(color);

    }
}


package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;
import android.support.design.widget.FloatingActionButton;
import android.widget.SeekBar;

import com.softdragon.omniwatch.R;

/**
 * Created by Dawid on 2016-03-10.
 */
public class FloatingActionButtonBinding {
    @BindingAdapter("ambitionMode")
    public static void setAmbitionMode(FloatingActionButton button, boolean src) {
        button.setImageResource(src? R.mipmap.zegar:R.mipmap.oko);

    }
}

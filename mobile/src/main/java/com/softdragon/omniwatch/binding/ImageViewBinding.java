package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.widget.ImageView;

/**
 * Created by Dawid on 2016-03-12.
 */
public class ImageViewBinding {
    @BindingAdapter("src")
    public static void setImageView(ImageView view, Drawable src) {
        view.setImageDrawable(src);

    }
}

package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;
import android.widget.SeekBar;

/**
 * Created by Dawid on 2016-02-09.
 */
public class SeekBarBinding {
    @BindingAdapter("value_listener")
    public static void setValueListener(SeekBar bar, SeekBar.OnSeekBarChangeListener listener) {
        bar.setOnSeekBarChangeListener(listener);
    }

    @BindingAdapter("value")
    public static void setValue(SeekBar bar, int value) {
        if (bar.getProgress() != value)
            bar.setProgress(value);
    }
}

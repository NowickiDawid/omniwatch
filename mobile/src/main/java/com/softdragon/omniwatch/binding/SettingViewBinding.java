package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;

import com.softdragon.omniwatch.fragment.viewmodel.SettingViewModel;
import com.softdragon.omniwatch.view.SettingView;

/**
 * Created by Dawid on 2016-06-24.
 */
public class SettingViewBinding {
    @BindingAdapter("viewModel")
    public static void setViewModel(SettingView settingView, SettingViewModel viewModel) {
        settingView.setViewModel(viewModel);
    }
}

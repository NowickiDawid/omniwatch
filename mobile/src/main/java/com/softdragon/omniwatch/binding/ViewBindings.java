package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;
import android.view.View;

/**
 * Created by Dawid on 2016-05-12.
 */
public class ViewBindings {

    @BindingAdapter("visibility")
    public static void setVisibility(View view, int visibility) {
        view.setVisibility(visibility);
    }
    @BindingAdapter("selected")
    public static void setSelected(View view, boolean isSelected) {
        view.setSelected(isSelected);
    }
}

package com.softdragon.omniwatch.binding;

import android.databinding.BindingAdapter;

import com.softdragon.omniwatch.view.OmniWatchPreview;

/**
 * Created by Dawid on 2016-02-14.
 */
public class WatchPrevBinding {
    @BindingAdapter("ambitionMode")
    public static void setAmbitionMode(OmniWatchPreview view, boolean isAmbitionMode) {
        view.setAmbientMode(isAmbitionMode);
    }
    @BindingAdapter("isRound")
    public static void setRound(OmniWatchPreview view, boolean isRound) {
        view.setWatchScreen(isRound,0);
    }
}

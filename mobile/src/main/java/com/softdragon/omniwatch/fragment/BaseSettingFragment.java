package com.softdragon.omniwatch.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.softdragon.omniwatch.fragment.viewmodel.SettingViewModel;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;

/**
 * Created by Dawid on 2016-07-10.
 */
public abstract class BaseSettingFragment extends Fragment{
    protected SettingFragment.ParamsListener mParamsListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mParamsListener = (SettingFragment.ParamsListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mParamsListener = null;
    }

}

package com.softdragon.omniwatch.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdragon.omniwatch.databinding.FragmentPageSettingBinding;
import com.softdragon.omniwatch.fragment.viewmodel.PageSettingFragmentViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dawid on 2016-07-10.
 */
public abstract class PageSettingFragment extends BaseSettingFragment  implements PartSettingFragment.PartSettingFragmentListener {
    private PageSettingFragmentViewModel mModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentPageSettingBinding binding = FragmentPageSettingBinding.inflate(inflater);
        mModel = new PageSettingFragmentViewModel(mParamsListener);
        binding.setViewModel(mModel);
        setupViewPager(binding.viewpager);
        binding.tabs.setupWithViewPager(binding.viewpager);
        return binding.getRoot();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        createPages(adapter);
        viewPager.setAdapter(adapter);
    }

    protected abstract void createPages(ViewPagerAdapter adapter);

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

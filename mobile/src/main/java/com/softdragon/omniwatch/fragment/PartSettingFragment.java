package com.softdragon.omniwatch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdragon.omniwatch.databinding.FragmentPageSettingBinding;
import com.softdragon.omniwatch.databinding.FragmentPartSettingBinding;
import com.softdragon.omniwatch.databinding.FragmentSettingBinding;
import com.softdragon.omniwatch.fragment.viewmodel.SettingViewModel;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;

/**
 * Created by Dawid on 2016-02-14.
 */
public class PartSettingFragment extends BaseSettingFragment implements SettingViewModel.SettingViewModelListener {
    private static final String INDEX = "INDEX_KEY";
    private SettingViewModel mModel;
    private PartSettingFragmentListener mPartSettingFragmentListener;

    public static PartSettingFragment newInstance(int index) {

        Bundle args = new Bundle();
        args.putInt(INDEX, index);
        PartSettingFragment fragment = new PartSettingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPartSettingFragmentListener = (PartSettingFragmentListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mPartSettingFragmentListener = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentPartSettingBinding binding = FragmentPartSettingBinding.inflate(inflater);
        mModel = new SettingViewModel(mParamsListener, this);
        binding.setModel(mModel);
        return binding.getRoot();
    }

    @Override
    public ColorParamsAdapter getColorParamsAdapter() {
        return mPartSettingFragmentListener.getColorParamsAdapter(getArguments().getInt(INDEX));
    }

    public interface PartSettingFragmentListener {
        ColorParamsAdapter getColorParamsAdapter(int index);
    }
}

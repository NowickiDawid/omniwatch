package com.softdragon.omniwatch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdragon.omniwatch.databinding.FragmentSettingBinding;
import com.softdragon.omniwatch.fragment.viewmodel.SettingViewModel;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;

/**
 * Created by Dawid on 2016-02-14.
 */
public class SettingFragment extends BaseSettingFragment implements SettingViewModel.SettingViewModelListener {
    private SettingViewModel mModel;
    private WatchParamAdapterImp mWatchParamAdapterImp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentSettingBinding binding = FragmentSettingBinding.inflate(inflater);
        mWatchParamAdapterImp = new WatchParamAdapterImp(getActivity());
        mModel = new SettingViewModel(mParamsListener, this);
        binding.setModel(mModel);
        return binding.getRoot();
    }

    @Override
    public ColorParamsAdapter getColorParamsAdapter() {
        return mWatchParamAdapterImp.getAdapter();
    }

    public interface ParamsListener {
        Params getParams();

        void setParams(Params params);

        void refreshParams();

    }
}

package com.softdragon.omniwatch.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.softdragon.omniwatch.databinding.FragmentWatchFaceBinding;
import com.softdragon.omniwatch.fragment.viewmodel.WatchFaceFragmentViewModel;
import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.DeviceSettingsAdapter;
import com.softdragon.omniwatch.view.OmniWatchPreview;
import com.softdragon.omniwatch.viewmodel.MainActivityViewModel;
import com.softdragon.omniwatch.viewmodel.MainActivityViewModel.OnDeviceSettingsListener;

/**
 * Created by Dawid on 2016-02-14.
 */
public class WatchFaceFragment extends Fragment {
    private static final String TAG = "WatchFaceFragment";
    private WatchFaceFragmentViewModel mModel = new WatchFaceFragmentViewModel();
    private OmniWatchPreview mPreview;
    private OnDeviceSettingsListener mOnDeviceSettingsListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mOnDeviceSettingsListener = (OnDeviceSettingsListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mOnDeviceSettingsListener = null;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentWatchFaceBinding binding = FragmentWatchFaceBinding.inflate(inflater);
        mPreview = binding.watchPreview;
        binding.setModel(mModel);
        refreshDeviceSettings(mOnDeviceSettingsListener.getDeviceSettings());
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        mPreview.onPause();
        super.onDestroyView();

    }

    public void refreshParams(Params params) {
        mPreview.setParams(params);
    }

    public void refreshDeviceSettings(DeviceSettings settings) {
        OmniLog.i(TAG, "refreshDeviceSettings" + DeviceSettingsAdapter.isRoundDevice(settings));
        mModel.isRound.set(DeviceSettingsAdapter.isRoundDevice(settings));
        mPreview.setWatchScreen(DeviceSettingsAdapter.isRoundDevice(settings), 0);
    }


}

package com.softdragon.omniwatch.fragment.viewmodel;

import android.databinding.BaseObservable;
import android.view.View;

import com.softdragon.omniwatch.fragment.SettingFragment;

/**
 * Created by Dawid on 2016-07-10.
 */
public class BaseSettingViewModel extends BaseObservable {
    protected final SettingFragment.ParamsListener mParamsListener;

    public BaseSettingViewModel(SettingFragment.ParamsListener paramsListener) {
        mParamsListener = paramsListener;
        refreshParams();
    }

    protected void refreshParams() {
        mParamsListener.refreshParams();
    }

    public View.OnClickListener getOnClickSendToWatch() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mParamsListener.setParams(mParamsListener.getParams());
            }
        };
    }
}

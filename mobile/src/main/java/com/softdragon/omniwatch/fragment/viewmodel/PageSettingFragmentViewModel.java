package com.softdragon.omniwatch.fragment.viewmodel;

import android.databinding.BaseObservable;

import com.softdragon.omniwatch.fragment.SettingFragment;

/**
 * Created by Dawid on 2016-07-10.
 */
public class PageSettingFragmentViewModel extends BaseSettingViewModel {
    public PageSettingFragmentViewModel(SettingFragment.ParamsListener paramsListener) {
        super(paramsListener);
    }
}

package com.softdragon.omniwatch.fragment.viewmodel;

import android.databinding.Bindable;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;

import com.softdragon.omniwatch.BR;
import com.softdragon.omniwatch.fragment.SettingFragment;
import com.softdragon.omniwatch.view.ColorPickerView;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;

/**
 * Created by Dawid on 2016-02-14.
 */
public class SettingViewModel extends BaseSettingViewModel {
    private static final String TAG = "SettingViewModel";
    private final ColorParamsAdapter mColorAdapter;

    public SettingViewModel(SettingFragment.ParamsListener paramsListener, SettingViewModelListener settingViewModelListener) {
        super(paramsListener);
        mColorAdapter = settingViewModelListener.getColorParamsAdapter();
    }


    @Bindable
    public int getHua() {
        return (int) ((mColorAdapter.getHua(mParamsListener.getParams()) / 0.25f));
    }

    @Bindable
    public boolean getBlackMode() {
        return mColorAdapter.getBlackMode(mParamsListener.getParams());
    }

    @Bindable
    public boolean getIs12Able() {
        return !getBlackMode() && mColorAdapter.is12Able();
    }

    @Bindable
    public boolean getIsBlack2Able() {
        return mColorAdapter.isBlackAble();
    }

    @Bindable
    public SeekBar.OnSeekBarChangeListener getOnHuaChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    setHua(progress * 0.25f);
                    refreshParams();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
    }


    @Bindable
    public int getSaturation() {
        return (int) (mColorAdapter.getSaturation(mParamsListener.getParams()) * 100f);
    }

    @Bindable
    public boolean getHour12() {
        return mColorAdapter.get12Hour(mParamsListener.getParams());
    }

    @Bindable
    public SeekBar.OnSeekBarChangeListener getOnSaturationChangeListener() {
        return new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    setSaturation(progress / 100.0f);
                    refreshParams();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };
    }

    @Bindable
    public CheckBox.OnCheckedChangeListener getOn12HourCheckedChangeListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mColorAdapter.set12Hour(isChecked, mParamsListener.getParams());
                refreshParams();
            }
        };
    }
    @Bindable
    public CheckBox.OnCheckedChangeListener getOnBlackCheckedChangeListener() {
        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mColorAdapter.setBlackMode(isChecked, mParamsListener.getParams());
                refreshParams();
                notifyPropertyChanged(BR.blackMode);
            }
        };
    }

    @Bindable
    public View.OnClickListener getOnColorColorPicker() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(v instanceof ColorPickerView))
                    return;
                ColorPickerView colorPickerView = (ColorPickerView) v;
                int color = colorPickerView.getColor();
                setColor(color);
            }

        };
    }

    private void setColor(int color) {
        float hsv[] = new float[3];
        Color.colorToHSV(color, hsv);
        setHua(hsv[0]);
        setSaturation(hsv[1]);
        refreshParams();
        notifyPropertyChanged(BR.saturation);
        notifyPropertyChanged(BR.hua);
        Log.d(TAG, String.format("Color %f %f %f", hsv[0], hsv[1], hsv[2]));
    }

    private void setSaturation(float saturation) {
        mColorAdapter.setSaturation(saturation, mParamsListener.getParams());
    }

    private void setHua(float hua) {
        mColorAdapter.setHua(hua, mParamsListener.getParams());
    }

    public interface SettingViewModelListener {
        ColorParamsAdapter getColorParamsAdapter();
    }
}

package com.softdragon.omniwatch.fragment.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableBoolean;
import android.view.View;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Wearable;
import com.softdragon.omniwatch.fragment.WatchFaceFragment;

/**
 * Created by Dawid on 2016-02-14.
 */
public class WatchFaceFragmentViewModel extends BaseObservable {
    public final ObservableBoolean ambitionMode =new ObservableBoolean();
    public final ObservableBoolean isRound =new ObservableBoolean(false);

    @Bindable
    public View.OnClickListener getOnClickAmbitionMode() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ambitionMode.set(!ambitionMode.get());
            }
        };
    }


}

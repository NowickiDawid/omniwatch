package com.softdragon.omniwatch.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.softdragon.omniwatch.R;
import com.softdragon.omniwatch.view.viewmodel.ColorPickerViewModel;

/**
 * Created by Dawid on 2016-03-06.
 */
public class ColorPickerView extends View {
    private static final int SHADOW_SIZE = 4;
    private Paint mPaint;
    private ColorPickerViewModel mModel;
    private Bitmap mShadows;
    private Paint mPaintShadow;

    public ColorPickerView(Context context) {
        super(context);
        init();
    }


    public ColorPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        createPaintShadow();
        mModel = new ColorPickerViewModel();
        mShadows = BitmapFactory.decodeResource(getResources(), R.drawable.shadow_settings);
    }

    private void createPaintShadow() {
        mPaintShadow = new Paint();
        mPaintShadow.setAntiAlias(true);
        mPaintShadow.setColor(Color.argb(100, 0, 0, 0));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int size = (canvas.getHeight() < canvas.getWidth() ? canvas.getHeight() : canvas.getWidth()) - SHADOW_SIZE * 2;
        int x = (getWidth() - size) / 2;
        int y = (getHeight() - size) / 2;
        x += SHADOW_SIZE;
        y += SHADOW_SIZE;
        canvas.drawOval(new RectF(x, y, x + size, y + size), mPaintShadow);
        x -= SHADOW_SIZE;
        y -= SHADOW_SIZE;
        canvas.drawOval(new RectF(x, y, x + size, y + size), mPaint);
    }

    public void setColor(int color) {
        mModel.setColor(color);
        mPaint.setColor(mModel.getColor());
        invalidate();
    }

    public int getColor() {
        return mModel.getColor();
    }
}

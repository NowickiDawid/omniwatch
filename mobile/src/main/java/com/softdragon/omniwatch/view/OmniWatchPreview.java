package com.softdragon.omniwatch.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.viewmodel.OmniWatchViewModel;

/**
 * Created by Dawid on 2016-02-07.
 */
public class OmniWatchPreview extends View implements OmniWatchView.OnWatchListener {
    OmniWatchView mWatch;
    private boolean mIsAmbientMode;

    public OmniWatchPreview(Context context) {
        super(context);
        init();
    }

    public OmniWatchPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        mWatch = new OmniWatchView(this, new OmniWatchViewModel());
        mWatch.onCreate();
    }

    public void setWatchScreen(boolean isRound, int bottomChin) {
        mWatch.setWatchScreenParams(isRound, bottomChin);
        mWatch.onRecreate();
    }

    public void onResume() {
        mWatch.onCreate();
    }

    public void onPause() {
        mWatch.onDestroy();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getMeasuredWidth() != 0 && getMeasuredHeight() != 0) {
            mWatch.onSurfaceChange(getMeasuredWidth(), getMeasuredHeight());
            invalidate();
        }
    }

    @Override
    protected void onVisibilityChanged(View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (visibility == VISIBLE)
            mWatch.updateTimer();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mWatch.onDraw(canvas, new Rect(0, 0, canvas.getWidth(), canvas.getHeight()));
    }

    public void setAmbientMode(boolean ambientMode) {
        mIsAmbientMode = ambientMode;
        mWatch.setAmbientMode(mIsAmbientMode);
    }

    @Override
    public boolean isInAmbientMode() {
        return mIsAmbientMode;
    }

    @Override
    public boolean shouldTimerBeRunning() {
        return true;
    }


    public void setParams(Params params) {
        mWatch.setParams(params);
    }
}

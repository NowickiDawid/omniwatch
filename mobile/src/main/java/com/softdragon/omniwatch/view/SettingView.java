package com.softdragon.omniwatch.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.softdragon.omniwatch.R;
import com.softdragon.omniwatch.databinding.ViewSettingsBinding;
import com.softdragon.omniwatch.fragment.SettingFragment;
import com.softdragon.omniwatch.fragment.viewmodel.SettingViewModel;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;

/**
 * Created by Dawid on 2016-06-23.
 */
public class SettingView extends RelativeLayout   {
    public SettingViewModel mModel;
    private ViewSettingsBinding mBinder;

    public SettingView(Context context) {
        super(context);
        init(context);
    }

    public SettingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mBinder = ViewSettingsBinding.inflate(LayoutInflater.from(context), this, true);

    }

    public void setViewModel(SettingViewModel viewModel) {
        mModel = viewModel;
        mBinder.setViewModel(mModel);
    }

}

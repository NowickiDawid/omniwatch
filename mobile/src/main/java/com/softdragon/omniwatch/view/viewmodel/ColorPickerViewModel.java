package com.softdragon.omniwatch.view.viewmodel;

import android.graphics.Color;

/**
 * Created by Dawid on 2016-03-06.
 */
public class ColorPickerViewModel {
    private int mColor = Color.rgb(0, 0, 0);
    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        mColor = color;
    }
}

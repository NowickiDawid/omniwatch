package com.softdragon.omniwatch.viewmodel;

import android.content.Context;

import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.WatchFaceBaseImp;
import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;
import com.softdragon.omniwatch.library.sychronize.omni.OmniSynData;

/**
 * Created by Dawid on 2016-03-12.
 */
public class MainActivityViewModel {
    private final OnDeviceSettingsListener mListener;
    private OmniSynData mOmniSynData;
    private Context mContext;
    private WatchParamAdapterImp mWatchParamAdapterImp;

    public MainActivityViewModel(OnDeviceSettingsListener listener) {
        mListener = listener;
    }


    public void onCreate(Context context) {
        mContext = context;
        init();
        mOmniSynData.onCreate();
        mWatchParamAdapterImp = new WatchParamAdapterImp(context);
    }

    private void init() {
        mOmniSynData = new OmniSynData(mContext, new OmniSynData.ReceiverListener() {
            @Override
            public void receiverParams(Params params) {

            }

            @Override
            public void receiverDeviceSettings(DeviceSettings device) {
                mListener.onDeviceSettingChanges(device);
            }
        });
    }

    public void onResume() {
        mOmniSynData.onResume();
    }

    public void onPause() {
        mOmniSynData.onPause();
    }


    public Params getParams() {
        return mOmniSynData.getParams();
    }

    public void setParams(Params params) {
        mOmniSynData.setParams(params);
    }

    public DeviceSettings getDeviceSettings() {
        return mOmniSynData.getDeviceSettings();
    }

    public ColorParamsAdapter getColorParamsAdapter(int index) {
        return mWatchParamAdapterImp.getColorParamsAdapter(index);
    }

    public interface OnDeviceSettingsListener {
        void onDeviceSettingChanges(DeviceSettings deviceSettings);

        DeviceSettings getDeviceSettings();
    }
}

package com.softdragon.omniwatch.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.softdragon.omniwatch.R;
import com.softdragon.omniwatch.library.presenter.WatchParamAdapterImp;
import com.softdragon.omniwatch.library.presenter.params.ColorParamsAdapter;

/**
 * Created by Dawid on 2016-07-10.
 */
public class SecondPageSettingFragment extends PageSettingFragment {
    private WatchParamAdapterImp mWatchParamAdapterImp;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWatchParamAdapterImp = new WatchParamAdapterImp(getContext());
    }

    @Override
    protected void createPages(ViewPagerAdapter adapter) {
        adapter.addFragment(PartSettingFragment.newInstance(0), getString(R.string.title_main));
        adapter.addFragment(PartSettingFragment.newInstance(1), getString(R.string.title_seconds));
    }


    @Override
    public ColorParamsAdapter getColorParamsAdapter(int index) {
        switch (index) {
            case 0:
                return mWatchParamAdapterImp.getGlobal();
            case 1:
                return mWatchParamAdapterImp.getSeconds();
        }
        return null;
    }
}

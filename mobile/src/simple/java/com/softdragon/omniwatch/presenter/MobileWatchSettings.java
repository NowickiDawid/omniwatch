package com.softdragon.omniwatch.presenter;

import android.support.v4.app.Fragment;

import com.softdragon.omniwatch.fragment.PageSettingFragment;
import com.softdragon.omniwatch.fragment.SecondPageSettingFragment;
import com.softdragon.omniwatch.fragment.SettingFragment;

/**
 * Created by Dawid on 2016-06-23.
 */
public class MobileWatchSettings {
    public static Fragment getSettingFragment() {
        return new SecondPageSettingFragment();
    }
}

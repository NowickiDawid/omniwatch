/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softdragon.omniwatch;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

import com.softdragon.omniwatch.library.data.DeviceSettings;
import com.softdragon.omniwatch.library.data.OmniLog;
import com.softdragon.omniwatch.library.data.Params;
import com.softdragon.omniwatch.library.presenter.params.DeviceSettingsAdapter;
import com.softdragon.omniwatch.library.sychronize.omni.OmniSynData;
import com.softdragon.omniwatch.library.view.OmniWatchView;
import com.softdragon.omniwatch.library.viewmodel.OmniWatchViewModel;

/**
 * Analog watch face with a ticking second hand. In ambient mode, the second hand isn't shown. On
 * devices with low-bit ambient mode, the hands are drawn without anti-aliasing in ambient mode.
 */
public class OmniWatch extends CanvasWatchFaceService implements OmniWatchView.OnWatchListener {
    private OmniWatchEngine mOmniWatchEngine;

    @Override
    public Engine onCreateEngine() {
        mOmniWatchEngine = new OmniWatchEngine();
        return mOmniWatchEngine;
    }

    public void invalidate() {
        mOmniWatchEngine.invalidate();
    }

    @Override
    public boolean shouldTimerBeRunning() {
        return mOmniWatchEngine.isVisible() && !isInAmbientMode();
    }

    @Override
    public boolean isInAmbientMode() {
        return mOmniWatchEngine.isInAmbientMode();
    }

    @Override
    public Context getContext() {
        return this;
    }

    private OmniWatchView createView() {
        return new OmniWatchView(this, new OmniWatchViewModel());
    }

    private class OmniWatchEngine extends CanvasWatchFaceService.Engine {
        private static final String TAG = "OmniWatchEngine";
        OmniWatchView mOmniWatchView = createView();
        boolean mRegisteredTimeZoneReceiver = false;
        final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mOmniWatchView.changeTimeZone();
            }
        };
        private OmniSynData mOmniSynData;

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            super.onApplyWindowInsets(insets);
            OmniLog.i(TAG, "onApplyWindowInsets" + insets.isRound() + " " + insets.getSystemWindowInsetBottom());
            mOmniWatchView.setWatchScreenParams(insets.isRound(), insets.getSystemWindowInsetBottom());
            sendWatchFaceData();
        }

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);
            setWatchFaceStyle(creteWatchFaceStyle(OmniWatch.this));
            mOmniWatchView.onCreate();
            settingSynData();
        }

        private void settingSynData() {
            mOmniSynData = new OmniSynData(getContext(), new WatchReceiverListener());
            mOmniSynData.onCreate();
            mOmniSynData.onResume();
            mOmniWatchView.setParams(mOmniSynData.getParams());
        }


        public WatchFaceStyle creteWatchFaceStyle(Service service) {
            return new WatchFaceStyle.Builder(service)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .setAcceptsTapEvents(true)
                    .build();
        }

        @Override
        public void onDestroy() {
            mOmniWatchView.onDestroy();
            mOmniSynData.onPause();
            super.onDestroy();
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mOmniWatchView.setLowBitAmbientMode(properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false));
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            mOmniWatchView.onTimeTick();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            mOmniWatchView.setAmbientMode(inAmbientMode);
            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            mOmniWatchView.updateTimer();
        }

        /**
         * Captures tap event (and tap type) and toggles the background color if the user finishes
         * a tap.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    mOmniWatchView.onTouchScreen(x, y, eventTime);
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    mOmniWatchView.onTouchCancel(x, y, eventTime);
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    mOmniWatchView.onTap(x, y, eventTime);
                    break;
            }
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            mOmniWatchView.onDraw(canvas, bounds);

        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            mOmniWatchView.onSurfaceChange(width, height);
            super.onSurfaceChanged(holder, format, width, height);
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();
                mOmniWatchView.changeTimeZone();
                sendWatchFaceData();
            } else {
                unregisterReceiver();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            mOmniWatchView.updateTimer();
        }

        private void sendWatchFaceData() {
            if (!mOmniWatchView.isWatchScreenParamsSetted())
                return;
            DeviceSettings deviceSettings = new DeviceSettings();
            DeviceSettingsAdapter.setIsDeviceRound(mOmniWatchView.isScreenRound(), deviceSettings);
            mOmniSynData.setDeviceSettings(deviceSettings);
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            OmniWatch.this.registerReceiver(mTimeZoneReceiver, filter);

        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            OmniWatch.this.unregisterReceiver(mTimeZoneReceiver);
        }

        private class WatchReceiverListener implements OmniSynData.ReceiverListener {

            @Override
            public void receiverParams(Params params) {
                setParms(params);
            }

            @Override
            public void receiverDeviceSettings(DeviceSettings device) {

            }
        }

        private void setParms(Params params) {
            mOmniWatchView.setParams(params);
        }


    }


}
